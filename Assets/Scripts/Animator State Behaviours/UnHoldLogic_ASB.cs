﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnHoldLogic_ASB : StateMachineBehaviour
{
    Climbing.ClimbBehaviour cb;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (cb == null)
            cb = animator.transform.GetComponent<Climbing.ClimbBehaviour>();

        cb.UnHold();
    }
}

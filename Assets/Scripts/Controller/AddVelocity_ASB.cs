﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddVelocity_ASB : StateMachineBehaviour
{

    public float life = 0.4f;
    public float force = 6;
    public Vector3 direction;
    [Space]
    [Header("This will override the direction")]
    public bool useTransformForward;
    public bool additive;
    public bool onEnter;
    public bool onExit;
    [Header("When Ending Applying velocity! Not aim state")]
    public bool onEndClampVelocity;

    StateManager states;
    HandleMovement_Player ply;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (onEnter) {
            AddVelocity(animator);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (onExit) {
            AddVelocity(animator);
        }
    }

    private void AddVelocity(Animator animator) {
        if (useTransformForward && !additive)
            direction = animator.transform.forward;

        if (useTransformForward && additive)
            direction += animator.transform.forward;

        if (states == null)
            states = animator.transform.GetComponent<StateManager>();

        if (!states.isPlayer)
            return;

        if (ply == null)
            ply = animator.transform.GetComponent<HandleMovement_Player>();

        ply.AddVelocity(direction, life, force, onEndClampVelocity);
    }
}

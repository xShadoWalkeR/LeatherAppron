﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State {
    ThirdPerson,
    FirstPerson
}

public class StateManager : MonoBehaviour {
    [Header("Info")]
    public GameObject modelPrefab;
    public bool inGame;
    public bool isPlayer;
    public bool isGroundForward;

    [Header("Stats")]
    public float groundDistance = 0.6f;
    public float groundOffset = 0f;
    public float distanceTocheckForward = 0.5f;
    public float runSpeed = 11;
    public float walkSpeed = 3.5f;
    public float jumpForce = 15;
    public float airTimeThreshold = 0.8f;

    [Header("Inputs")]
    public float horizontal;
    public float vertical;
    public bool jumpInput;

    [Header("States")]
    public State currentState;
    public bool obstacleForward;
    public bool groundForward;
    public float groundAngle;
    [HideInInspector]
    public bool climbing;

    #region StateRequests
    [Header("State Requests")]
    public CharStates curState;
    public bool onGround;
    public bool run;
    public bool walk;
    public bool onLocomotion;
    public bool inAngle_MoveDir;
    public bool jumping;
    public bool canJump;
    public bool dummy;
    #endregion

    #region References
    [HideInInspector]
    public GameObject activeModel;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Rigidbody rBody;
    #endregion

    #region Variables
    [HideInInspector]
    public Vector3 moveDirection;
    public float airTime;
    [HideInInspector]
    public bool prevGround;
    [HideInInspector]
    public string groundTag;
    #endregion

    LayerMask ignoreLayers;

    public enum CharStates {
        idle,
        moving,
        onAir,
        hold
    }

    #region Init Phase
    public void Init() {
        climbing = false;
        inGame = true;
        CreateModel();
        SetupAnimator();
        AddControllerReferences();
        canJump = true;
        currentState = State.ThirdPerson;

        gameObject.layer = 8;
        ignoreLayers = ~(1 << 3 | 1 << 8);
    }

    void CreateModel() {
        activeModel = Instantiate(modelPrefab) as GameObject;
        activeModel.transform.parent = this.transform;
        activeModel.transform.localPosition = Vector3.zero;
        activeModel.transform.localEulerAngles = Vector3.zero;
        activeModel.transform.localScale = Vector3.one;
    }

    void SetupAnimator() {
        anim = GetComponent<Animator>();
        Animator childAnim = activeModel.GetComponent<Animator>();
        anim.avatar = childAnim.avatar;
        Destroy(childAnim);
    }

    void AddControllerReferences() {
        gameObject.AddComponent<Rigidbody>();
        rBody = GetComponent<Rigidbody>();
        rBody.angularDrag = 999;
        rBody.drag = 4;
        rBody.constraints = RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;
    }
    #endregion


    public void FixedTick() {
        if (!dummy) {
            obstacleForward = false;
            groundForward = false;
            onGround = OnGround();

            if (onGround) {
                Vector3 origin = transform.position;
                // Clear forward
                origin += Vector3.up * 0.75f;
                IsClear(origin, transform.forward, distanceTocheckForward, ref obstacleForward);
                IsGroundForward();
                if (!obstacleForward) {
                    // Is ground forward?
                    origin += transform.forward * 0.6f;
                    IsClear(origin, -Vector3.up, groundDistance * 3, ref groundForward);
                } else {
                    if (Vector3.Angle(transform.forward, moveDirection) > 30) {
                        obstacleForward = false;
                    }
                }
            }

            UpdateState();
            MonitorAirTime();
        }
    }

    public void RegularTick() {
        onGround = OnGround();
        if (dummy) {
            IsGroundForward();
        }
    }

    void UpdateState() {
        if (curState == CharStates.hold)
            return;

        if (horizontal != 0 || vertical != 0) {
            curState = CharStates.moving;
        } else {
            curState = CharStates.idle;
        }

        if (!onGround) {
            curState = CharStates.onAir;
        }
    }

    public bool OnGround() {
        bool r = false;
        if (curState == CharStates.hold)
            return false;

        Vector3 origin = transform.position + (Vector3.up * 0.55f);

        RaycastHit hit = new RaycastHit();
        bool isHit = false;
        FindGround(origin, ref hit, ref isHit);

        if (!isHit) {
            for (int i = 0; i < 4; i++) {
                Vector3 newOrigin = origin;
                switch (i) {
                    case 0: // Forward
                        newOrigin += Vector3.forward / 3;
                        break;
                    case 1: // Backwards
                        newOrigin -= Vector3.forward / 3;
                        break;
                    case 2: // Left
                        newOrigin -= Vector3.right / 3;
                        break;
                    case 3: // Right
                        newOrigin += Vector3.right / 3;
                        break;
                }

                FindGround(newOrigin, ref hit, ref isHit);

                if (isHit == true)
                    break;
            }
        }

        r = isHit;
        if (r != false) {
            Vector3 targetPosition = transform.position;
            targetPosition.y = hit.point.y + groundOffset;
            transform.position = targetPosition;
        }

        return r;
    }

    void IsGroundForward() {
        Vector3 origin = transform.position + (Vector3.up * 0.55f) + (transform.forward / 2);

        RaycastHit hit = new RaycastHit();
        bool isHit = false;
        Debug.DrawRay(origin, -Vector3.up * 0.7f, Color.blue);
        if (Physics.Raycast(origin, -Vector3.up * 0.7f, out hit, groundDistance)) {
            isHit = true;
        }

        if (!isHit) {
            isGroundForward = false;
        } else {
            isGroundForward = true;
        }
    }

    public void EnableController() {
        if (!climbing) {
            dummy = false;
            rBody.isKinematic = false;
            GetComponent<Collider>().isTrigger = false;
        }
    }

    public void DisableController() {
        dummy = true;
        rBody.isKinematic = true;
        GetComponent<Collider>().isTrigger = true;
    }

    void FindGround(Vector3 origin, ref RaycastHit hit, ref bool isHit) {
        Debug.DrawRay(origin, -Vector3.up * 0.5f, Color.red);
        if (Physics.Raycast(origin, -Vector3.up * 0.5f, out hit, groundDistance, ignoreLayers, QueryTriggerInteraction.Ignore)) {
            isHit = true;
            groundTag = hit.transform.tag;
        }
    }

    void IsClear(Vector3 origin, Vector3 direction, float distance, ref bool isHit) {
        RaycastHit hit;
        Debug.DrawRay(origin, direction * distance, Color.green);
        if (Physics.Raycast(origin, direction, out hit, distance, ignoreLayers, QueryTriggerInteraction.Ignore)) {
            isHit = true;
        } else {
            isHit = false;
        }

        if (obstacleForward) {
            Vector3 incomingVec = hit.point - origin;
            Vector3 reflectVect = Vector3.Reflect(incomingVec, hit.normal);
            float angle = Vector3.Angle(incomingVec, reflectVect);

            if (angle < 70) {
                obstacleForward = false;
            }
        }

        if (groundForward) {
            if (curState == CharStates.moving) {
                Vector3 p1 = transform.position;
                Vector3 p2 = hit.point;
                float diffY = p1.y - p2.y;
                groundAngle = diffY;
            }

            float targetIncline = 0;

            if (Mathf.Abs(groundAngle) > 0.3f) {
                if (groundAngle < 0)
                    targetIncline = 1;
                else
                    targetIncline = -1;
            }

            if (groundAngle == 0)
                targetIncline = 0;

            anim.SetFloat(Statics.incline, targetIncline, 0.3f, Time.deltaTime);
        }
    }

    void MonitorAirTime() {
        if (!jumping)
            anim.SetBool(Statics.onAir, !onGround);

        if (onGround) {
            if (prevGround != onGround) {
                anim.SetInteger(Statics.jumpType,
                    (airTime > airTimeThreshold) ?
                    (horizontal != 0 || vertical != 0) ? 2 : 1
                    : 0);
            }
            airTime = 0;
            anim.SetFloat("airTime", 0);
        } else {
            airTime += Time.deltaTime;
            anim.SetFloat("airTime", (airTime / 2));
        }

        prevGround = onGround;
    }

    public void LegFront() {
        Vector3 ll = anim.GetBoneTransform(HumanBodyBones.LeftFoot).position;
        Vector3 rl = anim.GetBoneTransform(HumanBodyBones.RightFoot).position;
        Vector3 rel_ll = transform.InverseTransformPoint(ll);
        Vector3 rel_rr = transform.InverseTransformPoint(rl);

        bool left = rel_ll.z > rel_rr.z;
        anim.SetBool(Statics.mirrorJump, left);
    }
}
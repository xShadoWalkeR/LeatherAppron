﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Statics {

    // Use of variable to contain strings that represent the animator variables
    #region hash
    public static string horizontal = "horizontal";
    public static string vertical = "vertical";
    public static string special = "special";
    public static string specialType = "specialType";
    public static string onLocomotion = "onLocomotion";
    public static string Horizontal = "Horizontal";
    public static string Vertical = "Vertical";
    public static string jumpType = "jumpType";
    public static string Jump = "Jump";
    public static string onAir = "onAir";
    public static string mirrorJump = "mirrorJump";
    public static string incline = "incline";
    public static string Fire3 = "Fire3";

    #endregion

    #region Functions

    public static int GetAimSpecialType(AnimSpecials i) {
        int r = 0;
        switch (i) {
            case AnimSpecials.runToStop:
                r = 11;
                break;
            case AnimSpecials.run:
                r = 10;
                break;
            case AnimSpecials.jump_idle:
                r = 21;
                break;
            case AnimSpecials.run_jump:
                r = 22;
                break;
        }

        return r;
    }
    #endregion
}

public enum AnimSpecials {
    run,
    runToStop,
    jump_idle,
    run_jump
}

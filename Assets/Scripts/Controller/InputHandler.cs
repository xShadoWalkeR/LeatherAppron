﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {
    StateManager states;
    [HideInInspector]
    public CameraHandle camManager;
    [Space(5)]
    [Header("First Person Camera")]
    public Camera f_Camera;
    [Space(5)]
    [Header("Third Person Camera")]
    public Camera t_Camera;
    [Space(5)]
    [Header("Canvas Camera")]
    public Camera c_Camera;
    [Space(5)]
    [SerializeField] public MouseLook m_MouseLook;
    [Space(5)]
    public HandleMovement_Player hMove;

    [HideInInspector]
    public bool disableMouseLook;

    private Vector3 offset;
    private float horizontal;
    private float vertical;

    // Start is called before the first frame update
    void Start()
    {
        // Set Offset for first person cam
        offset = Vector3.zero;
        offset.z += 0.1f;
        offset.y += 0.07f;

        // Add references
        gameObject.AddComponent<HandleMovement_Player>();

        // Get references
        camManager = CameraHandle.singleton;
        states = GetComponent<StateManager>();
        hMove = GetComponent<HandleMovement_Player>();

        camManager.target = this.transform;
        f_Camera.gameObject.SetActive(false);

        // Init in order
        states.isPlayer = true;
        states.Init();
        hMove.Init(states, this);
        disableMouseLook = false;

        FixPlayerMeshes();
    }

    void FixPlayerMeshes() {
        SkinnedMeshRenderer[] skinned = GetComponentsInChildren<SkinnedMeshRenderer>();
        for ( int i = 0; i < skinned.Length; i++) {
            skinned[i].updateWhenOffscreen = true;
        }
    }

    private void FixedUpdate() {
        states.FixedTick();
        UpdateStatesFromInput();
        hMove.Tick();
        if (states.currentState == State.FirstPerson) {
            UpdateCameraPosition();
        }
    }

    private void Update() {
        if (states.currentState == State.FirstPerson && !disableMouseLook) {
            RotateView();
        }
        states.RegularTick();
    }

    public void EnableFirstPerson() {
        f_Camera.gameObject.SetActive(true);
        Vector3 temp = Vector3.zero;
        temp.y = camManager.lookAngle;
        transform.eulerAngles = temp;
        m_MouseLook.Init(transform, f_Camera.transform);
        states.currentState = State.FirstPerson;
        t_Camera.gameObject.SetActive(false);
        c_Camera.transform.parent = f_Camera.transform;
        camManager.transform.parent = f_Camera.GetComponentInParent<Transform>();
        camManager.enabled = false;
        CanvasCameraPosition();

        // Updates the camera for the depthoffield
        GetComponent<Player>().UpdateDepthOfField();
    }

    public void DisableFirstPerson() {
        f_Camera.gameObject.SetActive(false);
        camManager.transform.parent = null;
        camManager.enabled = true;
        camManager.lookAngle = transform.eulerAngles.y;
        states.currentState = State.ThirdPerson;
        t_Camera.gameObject.SetActive(true);
        c_Camera.transform.parent = t_Camera.GetComponentInParent<Transform>();
        CanvasCameraPosition();

        // Updates the camera for the depthoffield
        GetComponent<Player>().UpdateDepthOfField();
    }

    private void CanvasCameraPosition() {
        c_Camera.transform.localPosition = Vector3.zero;
        c_Camera.transform.localEulerAngles = Vector3.zero;
    }

    private void UpdateCameraPosition() {
        f_Camera.transform.position = states.anim.GetBoneTransform(HumanBodyBones.Head).position;
        f_Camera.transform.localPosition += offset;
    }

    private void RotateView() {
        m_MouseLook.LookRotation(transform, f_Camera.transform);
    }

    private void UpdateStatesFromInput() {
        vertical = Input.GetAxis(Statics.Vertical);
        horizontal = Input.GetAxis(Statics.Horizontal);

        Vector3 v, h;

        if (states.currentState == State.FirstPerson) {
            v = f_Camera.transform.forward * vertical;
            h = f_Camera.transform.right * horizontal;
        } else {
            v = camManager.transform.forward * vertical;
            h = camManager.transform.right * horizontal;
        }

        v.y = 0;
        h.y = 0;

        states.horizontal = horizontal;
        states.vertical = vertical;

        Vector3 moveDir = (h + v).normalized;
        states.moveDirection = moveDir;
        states.inAngle_MoveDir = InAngle(states.moveDirection, 120);
        if (states.walk && horizontal != 0 || states.walk && vertical != 0) {
            states.inAngle_MoveDir = true;
        }

        states.onLocomotion = states.anim.GetBool(Statics.onLocomotion);
        HandleRun();

        states.jumpInput = Input.GetButton(Statics.Jump);
    }

    private bool InAngle(Vector3 targetDir, float angleTheshold) {
        bool r = false;
        float angle = Vector3.Angle(transform.forward, targetDir);

        if ( angle < angleTheshold) {
            r = true;
        }

        return r;
    }

    private void HandleRun() {
        bool runInput = Input.GetButton(Statics.Fire3);

        if (runInput) {
            states.walk = false;
            states.run = true;
            states.anim.SetBool("walk", states.walk);
        } else if (!runInput) {
            states.walk = true;
            states.run = false;
            states.anim.SetBool("walk", states.walk);
        } else if (states.dummy) {
            states.walk = false;
            states.run = false;
            states.anim.SetBool("walk", states.walk);
        }
            

        if (horizontal != 0 || vertical != 0) {
            states.run = runInput;
            states.anim.SetInteger(Statics.specialType, Statics.GetAimSpecialType(AnimSpecials.run));
        } else {
            if (states.run)
                states.run = false;
        }

        if (!states.inAngle_MoveDir && hMove.doAngleCheck)
            states.run = false;

        if (states.obstacleForward)
            states.run = false;

        if (states.run == false && states.walk == false) {
            states.anim.SetInteger(Statics.specialType, Statics.GetAimSpecialType(AnimSpecials.runToStop));
        }
    }
}

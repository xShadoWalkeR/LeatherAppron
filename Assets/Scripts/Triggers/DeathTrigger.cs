﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour
{
    public CompassHandler ch;
    public GameObject gates;

    private bool off;

    private AudioSource clip;
    private AudioSource[] gateSounds;

    private void Start() {
        off = false;
        clip = GetComponentInChildren<AudioSource>();
        gateSounds = gates.GetComponentsInChildren<AudioSource>();
    }

    private void OnTriggerExit(Collider other) {
        if (!off) {
            foreach (AudioSource a in gateSounds) {
                a.Play();
            }
            foreach (Animator a in gates.GetComponentsInChildren<Animator>()) {
                a.SetBool("Closed", true);
            }
            StartCoroutine(SpawnBody());
            off = true;
        }
    }

    private IEnumerator SpawnBody() {
        yield return new WaitForSeconds(5);
        clip.Play();
        ch.murdersHolder.transform.GetChild(0).gameObject.SetActive(true);
        ch.CheckMurders();
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }
}

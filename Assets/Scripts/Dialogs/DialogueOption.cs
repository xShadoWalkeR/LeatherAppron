﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DialogueOption
{
    public string text;
    public int destinationNodeID;
    [HideInInspector]
    public Button button;
}

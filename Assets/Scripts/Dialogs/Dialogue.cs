﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    public string npcName;
    public AudioSource myAudio;
    public List<DialogueNode> Nodes;

    public void TriggerInteraction(Player player) => FindObjectOfType<DialogueManager>().StartDialogue(npcName, Nodes, player, myAudio);
    public void EndDialogue() => FindObjectOfType<DialogueManager>().EndDialogue();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueNode
{
    public int nodeID;
    [TextArea(3, 5)]
    public string text;
    public AudioClip audio;
    public bool playerLine;
    public GameObject unlockable;

    public bool isChoice;
    public int nextNode;
    public bool endNode;
    public List<DialogueOption> options;
    
    public void NodeUnlocks(DialogueManager dm) {
        if (endNode) {
            dm.firstChat = false;
        }
        if (unlockable != null) {
            unlockable.SetActive(true);
        }
    }
}

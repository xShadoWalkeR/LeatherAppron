﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DialogueManager : MonoBehaviour {
    public Text nameText;
    public Text dialogueText;
    public GameObject dialogueBox;
    public GameObject choiceBox;
    public GameObject myPlayer;
    public Canvas canvas;
    public AudioSource playerAudio;

    private List<DialogueNode> nodes;
    private AudioSource npcAudio;
    public GameObject buttonPrefab;
    private GameObject[] buttons;
    private Player player;

    [HideInInspector]
    public bool gotClue = false;

    private bool waitForInput;
    [HideInInspector]
    public bool firstChat;

    private DialogueNode currentNode;

    private List<DialogueOption> options;
    private int nodeID;

    private bool isPlaying;
    private bool startTimer;
    private float timer;

    void Start() {
        waitForInput = false;
        firstChat = true;
    }

    public void StartDialogue(string npcName, List<DialogueNode> nodes, Player player, AudioSource npcAudio) {
        this.player = player;
        this.nodes = nodes;
        this.npcAudio = npcAudio;
        timer = 0;
        startTimer = false;
        isPlaying = false;

        switch (npcName) {
            case "Milford":
                if (gotClue)
                    LoadNode(19);
                else if (firstChat)
                    LoadNode(2);
                else if (!firstChat)
                    LoadNode(0);
                break;
            case "Russell":
                LoadNode(0);
                break;
        }

        player.DisablePlayer();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Update() {
        if (waitForInput) {
            if (Input.GetKeyDown(KeyCode.Return)) {
                isPlaying = false;
                startTimer = false;
                timer = 0;
                waitForInput = false;
                CheckNode();
            }
        }
        if (isPlaying) {
            if (currentNode.playerLine) {
                if (!playerAudio.isPlaying) {
                    isPlaying = false;
                    startTimer = true;
                }
            } else {
                if (!npcAudio.isPlaying) {
                    isPlaying = false;
                    startTimer = true;
                }
            }
        }
        if (startTimer) {
            timer += Time.deltaTime;
            if (timer >= 1) {
                startTimer = false;
                timer = 0;
                CheckNode();
            }
        }
    }

    private void CheckNode() {
        if (currentNode.endNode) {
            playerAudio.Stop();
            npcAudio.Stop();
            EndDialogue();
        } else {
            npcAudio.Stop();
            playerAudio.Stop();
            if (currentNode.options.Count != 0) {
                LoadChoices();
            } else {
                LoadNode(currentNode.nextNode, currentNode.options);
            }
        }
    }

    // Loads the node text and the buttons
    public void LoadNode(int nodeID, List<DialogueOption> options = null) {
        this.options = options;
        this.nodeID = nodeID;
        currentNode = nodes[nodeID];

        // Then we get the options from the new node
        foreach (DialogueNode dl in nodes) {
            if (dl.nodeID == nodeID) {
                this.options = dl.options;
                dl.NodeUnlocks(this); // Check if this chat unlocks somthing in the game
            }
        }

        // If we're coming from a previous dialogue we must first destroy the previous choice buttons
        if (options != null) {
            foreach (DialogueOption op in options) {
                Destroy(op.button.gameObject);
            }
        }
        
        choiceBox.gameObject.SetActive(false);
        dialogueBox.SetActive(true);
        dialogueText.text = currentNode.text;

        if (currentNode.audio != null) {
            if (currentNode.playerLine) {
                playerAudio.clip = currentNode.audio;
                playerAudio.Play();
            } else {
                npcAudio.clip = currentNode.audio;
                npcAudio.Play();
            }
            
        }

        isPlaying = true;
        waitForInput = true;
    }

    private void LoadChoices() {
        dialogueBox.SetActive(false);
        choiceBox.gameObject.SetActive(true);

        canvas.worldCamera = null;

        if (options != null) {
            Vector3 buttonPos = Vector3.zero;
            Text buttonText;

            int nOptions = options.Count;
            int buttonWidth = 500;

            buttons = new GameObject[nOptions];
            buttonPos.y = 240;

            for (int i = 0; i < nOptions; i++) {
                // Spawn Buttons with text
                //*******************************************************
                // Offset each button y by 50
                buttonPos.y -= 75;
                // Spawn a new button
                buttons[i] = Instantiate(buttonPrefab) as GameObject;
                // Set's a parent for the button
                buttons[i].transform.SetParent(choiceBox.transform);
                // Set button width
                buttons[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, buttonWidth);
                // Set button scale
                buttons[i].GetComponent<RectTransform>().localScale = Vector3.one;
                // Set button position
                buttonPos.x = buttons[i].GetComponent<RectTransform>().anchoredPosition.x;
                buttonPos.x += 960; // No clue (It just works 0o)
                                    // Set button position
                buttons[i].GetComponent<RectTransform>().anchoredPosition = buttonPos;
                // Change the button text
                buttonText = buttons[i].GetComponentInChildren<Text>();
                buttonText.text = options[i].text;
                //*******************************************************

                options[i].button = buttons[i].GetComponent<Button>();
            }

            foreach (DialogueOption op in options) {
                op.button.onClick.AddListener(delegate { LoadNode(op.destinationNodeID, options); });
            }
        }
    }
    
    public void EndDialogue() {
        
        if (npcAudio.gameObject.name == "Russell") player.AddLetter();
        player.EnablePlayer();

        player.inConversation = false;
        dialogueBox.gameObject.SetActive(false);
        choiceBox.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}

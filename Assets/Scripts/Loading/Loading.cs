﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Loading level main script
/// </summary>
public class Loading : MonoBehaviour {
    /** \brief Array of all the lights in the under bunker */
    System.Random rnd = new System.Random();
    /** \brief Knows if we need to decrease or increase the alpha for the continue text */
    private bool decreasse;
    /** \brief Index of the next scene to load */
    private int nextScene;
    /** \brief limits te number of runs the coroutine will have */
    private bool fRun = true;
    /** \brief Object that displays the loading information */
    public GameObject load;
    /** \brief Object that displays the Continue text */
    public GameObject pressContinue;
    /** \brief Array of all available background images for this scene */
    public Sprite[] sprites;
    /** \brief Array of all available lore texts */
    [TextArea(3, 10)]
    public string[] lores;
    /** \brief Background image for the scene */
    public Image image;
    /** \brief Random text that displays the lore */
    public Text loreText;
    /** \brief Show the progress of the load */
    public Slider loadingSlider;
    /** \brief Text to continue */
    public Text continueText;
    /** \brief Creates a new AsyncOperation (Used to load a level while not switching to it) */
    AsyncOperation async;
    
    /// <summary>
    /// Runs at the start
    /// </summary>
	void Start () {
        // Gets the index of the next scene using it's name
        nextScene = 1;
        // Choses a random lore text from the array to display
        //loreText.text = lores[rnd.Next(0, lores.Length)];
        // Choses a random image for the scene background
        image.sprite = sprites[rnd.Next(0, sprites.Length)];
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update () {
        // checks if the pressContinue is active
		if (pressContinue.activeSelf) {
            // If soo
            // Animates Press to Contiue
            Color pColor = continueText.color;
            // Check if the alpha value is at a certain level to decreasse or increasse it
            if (pColor.a >= 0.95f)
                decreasse = true;
            else if (pColor.a <= 0.01f)
                decreasse = false;
            // Decreasses or increasses the alpha depending on the value
            pColor.a += (decreasse ? -Time.deltaTime  : Time.deltaTime) * 0.5f;
            continueText.color = pColor;
            // If any key is pressed switch to the next scene
            if (Input.anyKeyDown)
                async.allowSceneActivation = true;
        }
        // Check if fRun is true
        if (fRun) {
            // If so..
            // Starts the coroutine LoadingScreen
            StartCoroutine(LoadingScreen());
            // Set fRun to false
            fRun = false;
        }
	}

    /// <summary>
    /// Coroutine to load the next level while in this scene
    /// </summary>
    /// <returns>Waits for 1 sec/ null</returns>
    IEnumerator LoadingScreen() {
        // Loads the next scene in a new async prossess
        async = SceneManager.LoadSceneAsync(nextScene);
        // Disables the Scene auto activation
        async.allowSceneActivation = false;

        // Runs in cycle while the scene is not loaded
        while (!async.isDone) {
            // Switched the value of the slider to display the current load progress
            loadingSlider.value = async.progress;
            // If the progress is 0.9f
            if ( async.progress == 0.9f) {
                // Fills the loading slider
                loadingSlider.value = 1f;
                // Waits 1 second
                yield return new WaitForSecondsRealtime(1);
                // Disables the loading text / slicer
                load.SetActive(false);
                // Enables the press to continue text
                pressContinue.SetActive(true);
                // Stops all coroutines
                StopAllCoroutines();
            }
            // Returns null
            yield return null;
        }
    }
}

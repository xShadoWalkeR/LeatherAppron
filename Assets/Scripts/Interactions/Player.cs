﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    #region VARIABLES

    // Turns around 180 degrees (eulerAngles)
    private static readonly Vector3 TURN_AROUND = new Vector3(0, 180, 0);

    public float itemDetectionRadius;
    public float smooth;
    public float lerpTime = 0.8f;
    public Transform playerCamera;
    public Transform playerHead;
    public CameraHandle camHandle;
    public CanvasManager canvasManager;
    public Transform objectInspector;
    [HideInInspector]
    public bool inConversation;
    public CameraCollision cc;
    public UnityEngine.Events.UnityEvent changeVolume;
    public Item letter;

    private Inventory inv;
    private StateManager states;
    private InputHandler iHandler;
    private GameObject tMenu;

    private bool zoomIn;
    private bool zoomOut;

    private bool canGrabItem;   // true when not analyzing an item
    private bool canDropItem;   // true when analyzing an item

    private Collider currentItem;

    private Vector3 lastCamPos;
    private Quaternion lastCamRot;

    private Vector3 currPos;
    private Quaternion currRot;

    private Vector3 lastItemPos;
    private Quaternion lastItemRot;

    private FadePlayer fadePlayer;
    private DepthOfFieldManager depthOfField;

    private Vector3 itemStartPos;   // Items' starter position
    private Vector3 tempV3;

    private Vector3 camStartPos;    // Camera's starter position
    private Quaternion camStartRot; // Camera's starter rotation

    private float currentLerpTime;
    private float perc;   // time percentage of all Lerps

    private bool isLerping;   // prevents 'E' key misclicks
    private bool isFaded; // Knows if the player is faded

    private float holdKeyTime;
    private float timePassed;
    private bool tabPressed; // only set to true when TAB is pressed AND the UI is disabled
    private bool mmbPressed;
    
    private Dialogue dialouge;
    public LayerMask ignoreLayers; // What layers to ignore on raycast

    private GameObject violin;
    private GameObject bow;
    private GameObject vi;

    #endregion

    void Start() {
        inv = new Inventory();
        states = GetComponent<StateManager>();
        iHandler = GetComponent<InputHandler>();
        tMenu = canvasManager.tabMenu;

        zoomIn = false;
        zoomOut = false;

        canGrabItem = true;   // starts as true, because the player isn't analyzing anything
        canDropItem = false;

        fadePlayer = GetComponent<FadePlayer>();
        depthOfField = playerCamera.GetComponent<DepthOfFieldManager>();

        isLerping = false;

        holdKeyTime = 1f;
        timePassed = 0f;
        tabPressed = false;
        mmbPressed = false;

        HideCursor();
    }

    void Update() {
        UpdateDepthOfField();
        CheckInput();
        CheckTabPressTime();
        if (states.currentState == State.ThirdPerson)
            CheckDistance();

        if (zoomIn) {

            UpdateLerpTime();

            MoveCameraIn();

            if (canGrabItem) GrabItem(currentItem);

        } else if (zoomOut) {

            UpdateLerpTime();

            MoveCameraOut();

            if (canDropItem) DropItem(currentItem);
        }
    }

    private void UpdateLerpTime() {

        currentLerpTime += Time.deltaTime;

        if (currentLerpTime >= lerpTime) {

            currentLerpTime = lerpTime;
        }

        perc = (currentLerpTime / lerpTime);
    }
    
    public void UpdateDepthOfField() {
        if (states.currentState == State.FirstPerson)
            depthOfField = GetComponentInChildren<Camera>().GetComponent<DepthOfFieldManager>();
        else
            depthOfField = playerCamera.GetComponent<DepthOfFieldManager>();
    }

    public void SavePlayer()
    {
        Saving.SavePlayer(this);
    }

    public void LoadPlayer()
    {
        PlayerData data = Saving.LoadPlayer();

        //level = data.level;

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        transform.position = position;
    }

    private bool CheckForNPC() {
        Vector3 origin = playerHead.position;

        RaycastHit hit = new RaycastHit();
        bool isHit = false;

        //Debug.DrawRay(origin, transform.forward, Color.yellow);
        if (Physics.Raycast(origin, transform.forward, out hit, 4f, ignoreLayers)) {
            dialouge = hit.transform.gameObject.GetComponent<Dialogue>();
            isHit = true;
        }

        return isHit;
    }

    private void CheckInput() {

        if (tMenu.gameObject.activeSelf) {

            if (Input.GetKeyDown(KeyCode.Tab)) {
                EnablePlayer();
                tMenu.gameObject.SetActive(false);
                canvasManager.HideItem(inv.items);
                depthOfField.enabled = false;
                Time.timeScale = 1f;
                HideCursor();
            }

            return;
        }

        if (Input.GetKeyDown(KeyCode.R)) {

            changeVolume.Invoke();

            if (!depthOfField.enabled && !isLerping && !tMenu.gameObject.activeSelf && !inConversation) {
                switch (states.currentState) {
                    case State.FirstPerson:
                        iHandler.DisableFirstPerson();
                        break;
                    case State.ThirdPerson:
                        if (!states.dummy)
                            iHandler.EnableFirstPerson();
                        break;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (inConversation) {
                inConversation = false;
                dialouge.EndDialogue();
                EnablePlayer();
                HideCursor();
            }
        }

        if (Input.GetButtonDown("Interact")) {

            if (CheckItems()) {
                currPos = GetCurrentPosition();
                currRot = GetCurrentRotation();
            }

            if (CheckForNPC() && !inConversation) {
                inConversation = true;
                dialouge.TriggerInteraction(this);
                DisablePlayer();
                ShowCursor();
            }

        } else if (Input.GetMouseButtonDown(2)) {

            mmbPressed = true;

            if (CheckItems()) {

                currPos = GetCurrentPosition();
                currRot = GetCurrentRotation();
            } else {
                if (states.currentState == State.ThirdPerson)
                    CheckViolin();
            }

        } else if (Input.GetKeyDown(KeyCode.Tab)) {
            tabPressed = true;

        } else if (Input.GetKeyUp(KeyCode.Tab) && timePassed < holdKeyTime && tabPressed) {
            if (states.currentState == State.FirstPerson) {
                DisablePlayer();
            }

            tabPressed = false;
            tMenu.gameObject.SetActive(true);
            canvasManager.invButton.Select();
            canvasManager.invButton.onClick.Invoke();
            depthOfField.enabled = true;
            Time.timeScale = 0f;
            timePassed = 0f;
            ShowCursor();
        }

        if (timePassed >= holdKeyTime) {
            if (states.currentState == State.FirstPerson) {
                DisablePlayer();
            }

            tabPressed = false;
            tMenu.gameObject.SetActive(true);
            canvasManager.suspButton.Select();
            canvasManager.suspButton.onClick.Invoke();
            depthOfField.enabled = true;
            Time.timeScale = 0f;
            timePassed = 0f;
            ShowCursor();
        }
    }

    private void CheckViolin() {
        Collider[] hitColliders = Physics.OverlapSphere(GetPlayerCenter(), itemDetectionRadius);

        for (int i = 0; i < hitColliders.Length; i++) {
            if (hitColliders[i].tag == "Violin") {
                states.DisableController();
                vi = hitColliders[i].gameObject;
                FindViolin();
                GetComponent<Animator>().SetBool("playViolin", true);
                StartCoroutine(EnableViolin());
            }
        }
    }

    private void FindViolin() {
        Transform[] gm = GetComponentsInChildren<Transform>(true);

        for (int i = 0; i < gm.Length; i++) {
            if (gm[i].tag == "Violin") {
                violin = gm[i].gameObject;
            } else if (gm[i].tag == "Bow") {
                bow = gm[i].gameObject;
            }
        }
    }

    private IEnumerator EnableViolin() {
        yield return new WaitForSeconds(0.5f);
        vi.SetActive(false);
        violin.SetActive(true);
        bow.SetActive(true);

    }

    public void DisableViolin() {
        GetComponent<Animator>().SetBool("playViolin", false);
        vi.SetActive(true);
        violin.SetActive(false);
        bow.SetActive(false);
        states.EnableController();
    }

    private void CheckTabPressTime() {

        if (tabPressed) {

            timePassed += Time.deltaTime;
        }
    }

    private void CheckDistance() {

        float dist = Vector3.Distance(playerHead.position, playerCamera.position);
        
        if (dist <= 1.75f) {
            
            if (zoomIn) {

                HidePlayer();
            }

        } else if (dist > 0.5f) {
            
            if (zoomOut || isFaded) {

                ShowPlayer();
            }
        }
    }

    private bool CheckItems() {

        if (isLerping) return false;
        
        Collider[] hitColliders = Physics.OverlapSphere(GetPlayerCenter(), itemDetectionRadius);

        for (int i = 0; i < hitColliders.Length; i++) {

            if (hitColliders[i].GetComponent<Item>()) {

                // save currentItem for use outside of this method
                currentItem = hitColliders[i];

                currentItem.GetComponent<Item>().curCam = playerCamera;

                if (currentItem.GetComponent<Item>().isPickable && !mmbPressed && canGrabItem) {

                    inv.AddItem(currentItem.GetComponent<Item>());
                    return false;
                }

                mmbPressed = false;

                if (canGrabItem) {

                    currentLerpTime = 0f;

                    DisablePlayer();

                    // MOVING THIS OUT OF HERE BUGS THE CAMERA AND TRANSPARENCY
                    lastCamPos = playerCamera.position;
                    lastCamRot = playerCamera.rotation;

                    SaveItemsPosAndRot();
                    SaveCamPosAndRot();

                    zoomIn = true;
                    zoomOut = false;
                    canDropItem = true;
                    isLerping = true;

                    return true;

                } else if (canDropItem) {

                    if (currentItem.GetComponent<Item>().isPickable) {

                        inv.AddItem(currentItem.GetComponent<Item>());
                    }

                    currentLerpTime = 0f;

                    itemStartPos = currentItem.transform.position;
                    SaveCamPosAndRot();

                    zoomIn = false;
                    zoomOut = true;
                    HideCursor();
                    canGrabItem = true;
                    isLerping = true;

                    return false;
                }
            }
        }

        return false;
    }

    // Saves Items' positions and rotations
    private void SaveItemsPosAndRot() {

        // Items'
        itemStartPos = currentItem.transform.position;

        if (states.currentState == State.FirstPerson)
            tempV3 = objectInspector.position;
        else if (states.currentState == State.ThirdPerson)
            tempV3 = playerHead.position + new Vector3(playerHead.forward.x / 2.75f, 0.1f, playerHead.forward.z / 2.5f);

        // Save last pos and rot (Items')
        lastItemPos = currentItem.transform.position;
        lastItemRot = currentItem.transform.rotation;
    }

    // Saves Camera's positions and rotations
    private void SaveCamPosAndRot() {
        camStartPos = playerCamera.position;
        camStartRot = playerCamera.rotation;

    }

    private Vector3 GetPlayerCenter() {

        return new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z);
    }

    private void OnDrawGizmos() {

        Vector3 playerCenter = new Vector3(transform.position.x, transform.position.y + 0.9f, transform.position.z);
        Gizmos.color = Color.red;

        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(playerCenter, itemDetectionRadius);
    }

    private void GrabItem(Collider item) {

        item.transform.position = Vector3.Lerp(itemStartPos, tempV3, perc);

        // rotate towards player
        item.transform.rotation = GetCurrentRotation();
        item.transform.Rotate(TURN_AROUND, Space.Self);

        // turn on dof
        depthOfField.enabled = true;

        if (item.transform.position == tempV3) {
            canGrabItem = false;
            isLerping = false;
        }
    }

    private void DropItem(Collider item) {

        item.transform.position = Vector3.Lerp(itemStartPos, lastItemPos, perc);
        item.transform.rotation = Quaternion.Lerp(item.transform.rotation, lastItemRot, perc);

        // turn off dof
        depthOfField.enabled = false;

        if (item.transform.position == lastItemPos) {

            canDropItem = false;
            isLerping = false;
            currentItem = null;
        }
    }

    public void EnablePlayer() {
        if (states.currentState == State.FirstPerson) {
            iHandler.disableMouseLook = false;
            camHandle.gameObject.SetActive(true);
        } else {
            camHandle.enabled = true;
            states.EnableController();
        }
    }

    public void DisablePlayer() {
        if (states.currentState == State.FirstPerson) {
            iHandler.disableMouseLook = true;
            camHandle.gameObject.SetActive(false);
        } else {
            camHandle.enabled = false;
            states.DisableController();
        }
    }

    private void ShowPlayer() {
        isFaded = false;
        fadePlayer.SetMaterialsOpaque();
    }

    private void HidePlayer() {
        isFaded = true;
        fadePlayer.SetMaterialsTransparent();
    }

    private void MoveCameraIn() {

        cc.enabled = false;

        playerCamera.position = Vector3.Lerp(camStartPos, currPos, perc);
        playerCamera.rotation = Quaternion.Lerp(camStartRot, currRot, perc);

        if (playerCamera.position == currPos) {

            zoomIn = false;
        }

        ShowCursor();
    }

    private void MoveCameraOut() {

        cc.enabled = true;

        playerCamera.position = Vector3.Lerp(camStartPos, lastCamPos, perc);
        playerCamera.rotation = Quaternion.Lerp(camStartRot, lastCamRot, perc);

        if (playerCamera.position == lastCamPos) {

            zoomOut = false;
            EnablePlayer();
        }
    }

    // Referring to playerHead
    private Vector3 GetCurrentPosition() {
        float currentX = playerHead.position.x;
        float currentY = playerHead.position.y;
        float currentZ = playerHead.position.z;

        return new Vector3(currentX, currentY + 0.1f, currentZ);
    }

    // Referring to playerHead
    private Quaternion GetCurrentRotation() {
        float currentY = playerHead.rotation.y;
        float currentW = playerHead.rotation.w;

        return new Quaternion(0, currentY, 0, currentW);
    }

    private void ShowCursor() {

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void HideCursor() {

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void AddLetter() {
        
        inv.AddItem(letter);
    }
}

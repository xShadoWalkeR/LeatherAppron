﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using TMPro;

public class PauseMenu : MonoBehaviour
{

    public static bool gamePause = false;
    public Player player;
    public GameObject pause;
    public GameObject thirdPerson;
    public AudioSource pauseMusic;

    public GameObject itemPrefab;
    public GameObject suspectPrefab;

    public GameObject items;
    public GameObject suspects;

    public AudioMixerSnapshot unpaused;
    public AudioMixerSnapshot paused;

    private CanvasManager cm;

    private void Start() {

        cm = CanvasManager.Instance;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!gamePause && (!cm.inventory.activeSelf || !cm.suspects.activeSelf))
            {
                Pause();
            }
            else if (gamePause && (!cm.inventory.activeSelf || !cm.suspects.activeSelf))
            {
                Resume();
            }
        }
    }

    public void Resume()
    {
        // The cursor will not be visible
        unpaused.TransitionTo(0f);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        pause.SetActive(false);
        Time.timeScale = 1f;
        player.EnablePlayer();
        gamePause = false;
    }

    public void Pause()
    {
        // The cursor will be visible
        paused.TransitionTo(0f);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        pause.SetActive(true);
        Time.timeScale = 0f;
        player.DisablePlayer();
        gamePause = true;
        pauseMusic.PlayDelayed(1.5f);
    }

    public void Save()
    {
        PlayerPrefs.SetString("Saveded", "true");
        PlayerPrefs.SetFloat("playerX", thirdPerson.transform.position.x);
        PlayerPrefs.SetFloat("playerY", thirdPerson.transform.position.y);
        PlayerPrefs.SetFloat("playerZ", thirdPerson.transform.position.z);

        SaveItems(items.transform);
        SaveSuspects(suspects.transform);
    }

    private void SaveItems(Transform items) {

        for (int i = 0; i < items.childCount; i++) {

            print(items.GetChild(i).GetChild(0).GetComponent<TMP_Text>().text);
            PlayerPrefs.SetString($"item{i}", items.GetChild(i).GetChild(0).GetComponent<TMP_Text>().text);
        }
    }

    private void SaveSuspects(Transform suspects) {

        for (int i = 0; i < suspects.childCount; i++) {

            print(suspects.GetChild(i).GetChild(i).GetComponent<TMP_Text>().text);
            PlayerPrefs.SetString($"suspect{i}", suspects.GetChild(i).GetChild(0).GetComponent<TMP_Text>().text);
        }
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        Debug.Log("Loading Menu...");
    }
    
    public void QuitGame()
    {
        Debug.Log("Quiting the Game? Pfff Looooser!");
        Application.Quit();
    }
}


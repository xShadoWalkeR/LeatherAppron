﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void StartGame()
    {
        PlayerPrefs.SetString("Loaded", "false");
        SceneManager.LoadScene("Loading");
    }

    public void LoadGame()
    {
        if (PlayerPrefs.HasKey("Saveded"))
        {
            PlayerPrefs.SetString("Loaded", "true");
            SceneManager.LoadScene("Prototype");
          
        }
    }

    public void QuitGame()
    {
        Debug.Log("Quit the game, see you next time ;D");
        Application.Quit();
    }


}

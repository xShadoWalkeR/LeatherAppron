﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clue : MonoBehaviour {
    [TextArea(3, 5)]
    public string clue;

    private Material originalM;
    private Color clueColor;

    private bool clueDiscovered;

    // Start is called before the first frame update
    void Start() {

        originalM = gameObject.GetComponent<Renderer>().material;
        clueColor = gameObject.GetComponent<Renderer>().material.color;

        clueDiscovered = false;
    }

    private void OnMouseEnter() {

        if (!clueDiscovered) {

            originalM.color = new Color(clueColor.r, clueColor.g, clueColor.b, 0.5f);
        }
    }

    private void OnMouseExit() {

        if (!clueDiscovered) {

            originalM.color = new Color(clueColor.r, clueColor.g, clueColor.b, 0f);
        }
    }

    private void OnMouseOver() {

        if (Input.GetMouseButtonDown(0)) {

            clueDiscovered = true;
            originalM.color = new Color(clueColor.r, clueColor.g, clueColor.b, 1f);
            GetComponentInParent<Item>().ShowClue(clue);
        }
    }
}

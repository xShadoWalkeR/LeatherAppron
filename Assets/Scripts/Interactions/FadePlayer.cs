﻿using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadePlayer : MonoBehaviour {
    private List<GameObject> children;

    private GameObject player;

    private StateManager sm;
    private GameObject top;

    private List<Material> cMaterials;
    private List<Color> cColors;

    private List<float> renderModes;

    private readonly float time = 1f;

    void OnEnable() {

        sm = GetComponent<StateManager>();

        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart() {
        yield return new WaitForSeconds(0.2f);
        player = transform.GetChild(4).gameObject;
        GetChildren();
        GetMesh();
    }

    private void GetChildren() {
        children = new List<GameObject>();
        Renderer[] temp = player.GetComponentsInChildren<Renderer>();

        foreach (Renderer t in temp) {
            if (t != null) {
                Material material = t.material;
                material = new Material(material);
                t.material = material;
                children.Add(t.gameObject);

            }
        }
    }

    private void GetMesh() {

        cMaterials = new List<Material>();
        cColors = new List<Color>();
        renderModes = new List<float>();

        for (int i = 0; i < children.Count; i++) {
            if (children[i].name == "top") {
                top = children[i];
            }
            if (children[i].GetComponent<Renderer>() != null) {
                cMaterials.Add((Material)children[i].GetComponent<Renderer>().material);
                cColors.Add(children[i].GetComponent<Renderer>().material.color);
                renderModes.Add(cMaterials[i].GetFloat("_Mode"));
            }
        }
    }

    public void SetMaterialsTransparent() {

        foreach (Material m in cMaterials) {

            m.SetFloat("_Mode", 2);

            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);

            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

            m.SetInt("_ZWrite", 0);

            m.DisableKeyword("_ALPHATEST_ON");

            m.EnableKeyword("_ALPHABLEND_ON");

            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");

            m.renderQueue = 3000;
        }

        StopAllCoroutines();

        for (int i = 0; i < children.Count; i++) {
            Renderer renderer = children[i].GetComponent<Renderer>();
            if (renderer) {
                Material material = renderer.material;
                if (material) {
                    StartCoroutine(FadeAndShow(material, material.color, true));
                }         
            }           
        }
        if (top != null)
            top.SetActive(false);
    }

    private IEnumerator FadeAndShow(Material mat, Color color, bool fade = false) {
        if (fade) {
            float a = 1;
            while (a > 0.0f) {
                a = Mathf.Clamp01(a - Time.deltaTime * 15.0f);
                mat.SetColor("_Color", new Color(color.r, color.g, color.b, a));
                yield return null;
            }
        } else {
            float a = 0;
            while (a < 1.0f) {
                a = Mathf.Clamp01(a + Time.deltaTime * 15.0f);
                mat.SetColor("_Color", new Color(color.r, color.g, color.b, a));
                yield return null;
            }

            SetOpaque();
        }
    }

    public void SetMaterialsOpaque() {

        StopAllCoroutines();

        for (int i = 0; i < children.Count; i++) {
            Renderer renderer = children[i].GetComponent<Renderer>();
            if (renderer) {
                Material material = renderer.material;
                if (material) {
                    StartCoroutine(FadeAndShow(material, material.color, false));
                }
            }
        }

        if (top != null)
            top.SetActive(true);
    }

    private void SetOpaque() {   // OR CUTOUT

        int n = 0;

        foreach (Material m in cMaterials) {

            m.SetFloat("_Mode", renderModes[n]);

            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);

            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);

            m.SetInt("_ZWrite", 1);

            if (renderModes[n] == 0) {
                m.DisableKeyword("_ALPHATEST_ON");
            }
            else {
                m.EnableKeyword("_ALPHATEST_ON");
            }

            m.DisableKeyword("_ALPHABLEND_ON");

            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");

            m.renderQueue = -1;

            if (renderModes[n] != 0) {
                Color color = new Color(m.color.r, m.color.g, m.color.b, 1.0f);
                m.color = color;
            }

            n++;
        }
    }
}

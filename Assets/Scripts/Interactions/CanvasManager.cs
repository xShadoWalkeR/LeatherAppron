﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasManager : MonoBehaviour {
    
    #region Singleton
        
    public static CanvasManager Instance { get; private set; }

    void Awake() {

        // Set the instance as the entirety of the class if it doesn't exist yet
        Instance = this;

        tabMenu = transform.GetChild(0).gameObject;
    }
    
    #endregion

    public Button invButton;
    public Button suspButton;

    public GameObject inventory;
    public GameObject suspects;

    public Transform itemsList;
    public GameObject itemPrefab;

    public Transform suspectsList;
    public GameObject suspectPrefab;
    
    public Transform anchorPoint; // item's set position

    [HideInInspector]
    public GameObject tabMenu;

    private Transform canvasCamera;

    public GameObject dialogueBox;
    public Camera camera;

    public GameObject textField;

    public GameObject player;

    private GameObject currentItem;
    
    private void Start()
    {
        if (PlayerPrefs.HasKey("Loaded") && PlayerPrefs.GetString("Loaded") == "true")
        {
            Vector3 trans = Vector3.zero;
            trans.x = PlayerPrefs.GetFloat("PlayerX");
            trans.y = PlayerPrefs.GetFloat("PlayerY");
            trans.z = PlayerPrefs.GetFloat("PlayerZ");

            player.transform.position = trans;
        }
    }

    private void Update() {
        if (dialogueBox.activeSelf) {
            GetComponent<Canvas>().worldCamera = null;
        } else {
            GetComponent<Canvas>().worldCamera = camera;
        }
    }

    public void ShowInventory() {

        if (suspects) {
            
            suspects.SetActive(false);
        }

        textField.SetActive(false); // disable the item's description as well
        inventory.SetActive(true);
    }

    public void ShowSuspects() {

        if (inventory) {

            inventory.SetActive(false);
        }

        if (currentItem != null) currentItem.SetActive(false); // disable current active item
        suspects.SetActive(true);
    }

    public void CreateItemButton(string itemName, List<Item> inv) {

        GameObject item = Instantiate(itemPrefab, itemsList);

        item.GetComponentInChildren<TMP_Text>().text = itemName;

        item.GetComponent<Button>().onClick.AddListener(delegate { ShowItem(itemName, inv); });
    }

    public void ShowItem(string itemName, List<Item> inv) {

        canvasCamera = GetComponent<Canvas>().worldCamera.transform;

        HideItem(inv);

        foreach (Item i in inv) {

            if (i.actualName == itemName) {

                currentItem = i.gameObject;

                // > CHANGING THE 'textField' CHILD MEANS CHANGING THIS LINE OF CODE <
                // Assigns item's description to the text field and enables it
                textField.transform.GetChild(2).GetComponent<TMP_Text>().text = i.itemDescription;
                textField.SetActive(true);

                // Vector to subtract
                Vector3 correctSpot = anchorPoint.position - ((anchorPoint.position - canvasCamera.position)/2);

                // Set new position
                i.transform.position = correctSpot;
                
                // Set new rotation
                i.transform.rotation = Quaternion.LookRotation(-canvasCamera.forward, canvasCamera.up);

                i.gameObject.SetActive(true);

                return;
            }
        }
    }

    public void HideItem(List<Item> inv) {

        foreach (Item i in inv) {

            if (i.gameObject.activeSelf) {

                i.gameObject.SetActive(false);
                return;
            }
        }
    }
    
    public void AddSuspectToList(string suspectName) {

        if (!SuspectExists(suspectName)) {

            GameObject suspect = Instantiate(suspectPrefab, suspectsList);

            suspect.GetComponentInChildren<TMP_Text>().text = suspectName;
        }
    }

    // Verify if suspect is already in the list
    private bool SuspectExists(string suspectName) {

        foreach (TMP_Text t in suspectsList.GetComponentsInChildren<TMP_Text>()) {

            if (t.text == suspectName) {

                return true;
            }
        }

        return false;
    }
}

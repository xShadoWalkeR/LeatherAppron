﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory {

    // public for now (debugging purposes)
    public List<Item> items;

    private CanvasManager _canvasManager;

    // Start is called before the first frame update
    public Inventory() {
        items = new List<Item>();

        _canvasManager = CanvasManager.Instance;
    }

    public void AddItem(Item item) {

        // play sound?

        item.gameObject.SetActive(false);

        items.Add(item);

        AddItemToList(item);
    }

    // Checks if Inventory possesses a certain Item (to be used later)
    public bool HasItem(Item item) {

        foreach (Item i in items) {

            if (i == item) {

                return true;
            }
        }

        return false;
    }

    private void AddItemToList(Item item) {

        _canvasManager.CreateItemButton(item.actualName, items);
    }
}

﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class DepthOfFieldManager : MonoBehaviour {

    private DepthOfField dof;

    private PostProcessProfile post;

    void Awake() {
        
        post = GetComponent<PostProcessVolume>().profile;
        
        dof = post.GetSetting<DepthOfField>();
    }

    void OnEnable() {

        dof.enabled.value = true;
    }

    void OnDisable() {

        dof.enabled.value = false;
    }
}

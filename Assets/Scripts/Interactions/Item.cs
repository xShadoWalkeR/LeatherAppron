﻿using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour {

    // the ACTUAL item name
    public string actualName;

    // can go into inventory
    public bool isPickable;

    // has clue
    public bool hasClue;

    public float rotSpeed = 150;

    [TextArea]
    public string itemDescription;

    public GameObject clueBox;

    [HideInInspector]
    public Transform curCam;

    public DialogueManager dm;

    public Dialogue npc;

    private bool foundClue = false;

    private CanvasManager cm;

    public GameObject boss;

    void Start() {
        cm = CanvasManager.Instance;

        if (actualName == "Doll") {
            boss.SetActive(true);
            cm.gameObject.GetComponent<CompassHandler>().CheckActiveNPCs();
        }
    }

    private void Update() {

        CheckInput();
        curCam = Camera.main.transform;
    }

    private void CheckInput() {

        if (Input.GetMouseButtonDown(2) || Input.GetButtonDown("Interact")) {

            clueBox.SetActive(false);

            if (foundClue)
                GetComponentInChildren<GameObject>().SetActive(false);
        }
    }

    private void OnMouseDrag() {

        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

        transform.Rotate(curCam.up, -rotX, Space.World);
        transform.Rotate(curCam.right, rotY, Space.World);
    }

    public void ShowClue(string clue) {
        if (hasClue && !foundClue) {
            if (!clueBox.GetComponentInParent<CanvasManager>().inventory.activeSelf) {
                clueBox.GetComponentInChildren<Text>().text = clue;
                clueBox.SetActive(true);
                itemDescription += " " + clue;
                if (dm!= null) dm.gotClue = true;
            } else if (!foundClue) {
                foundClue = true;
                itemDescription += " " + clue;
                if (dm != null) dm.gotClue = true;
            }

            // All objects will have a glowing shader (as in AC: Odyssey)
            // https://assetstore.unity.com/packages/vfx/shaders/gem-shader-3
            // Set clue active, which was hidden (transparent?)
        }

        if (npc != null) {

            cm.AddSuspectToList(npc.npcName);
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompassHandler : MonoBehaviour {

    #region VARIABLES

    [Header("Compass UI elements")]
    public RawImage compassImg;
    public Transform playerCamera;

    [Header("Positions to calculate angles")]
    public Transform north;
    public Transform playerFeet;

    [Header("Parent of all markers")]
    public Transform markersParent;

    [Header("NPCs stuff")]
    public RawImage npcMarker;   // the prefab for the markers (NPC)
    public Transform npcsHolder;

    [Header("Murders stuff")]
    public RawImage murderMarker;
    public Transform murdersHolder;

    [Header("Canvas Elements")]
    public GameObject inventory;
    public GameObject pauseMenu;
    public GameObject dialogue;
    public GameObject compass;
    public GameObject dialogueBox;

    private Vector3 camFloor;
    private Vector3 feetFloor;
    private Vector3 u;          // feetFloor - camFloor;
    
    private Transform[] npcs;
    private List<RawImage> npcMarkers;

    private Transform[] murders;
    private List<RawImage> murderMarkers;
    #endregion

    void Start() {

        CheckNpcs();
        CheckMurders();
    }

    void Update() {

        ManageCompass();

        compassImg.uvRect = new Rect(UpdateNorthDirection() / 360, 0, 1, 1);

        if (npcs != null) {
            for (int i = 0; i < npcs.Length; i++) {
                if (npcs[i].gameObject.activeSelf)
                    npcMarkers[i].uvRect = new Rect(UpdateMarkerDirection(npcs[i].position) / 360, 0, 1, 1);
            }
        }

        if (murders != null) {
            for (int i = 0; i < murders.Length; i++) {
                if (murders[i].gameObject.activeSelf)
                    murderMarkers[i].uvRect = new Rect(UpdateMarkerDirection(murders[i].position) / 360, 0, 1, 1);
            }
        }
    }

    private float UpdateNorthDirection() {
        
        // cos Q = (u x v) / ||u|| x ||v||

        camFloor = new Vector3(playerCamera.position.x, 0, playerCamera.position.z);
        feetFloor = new Vector3(playerFeet.position.x, 0, playerFeet.position.z);

        u = feetFloor - camFloor;
        Vector3 v = north.position - camFloor;

        float devisor = (u.x * v.x) + (u.y * v.y) + (u.z * v.z);

        // M = Magnitude
        float uM = Vector3.Magnitude(u);
        float vM = Vector3.Magnitude(v);

        float dividend = uM * vM;

        // In degrees
        float angle = Mathf.Acos(devisor / dividend) * Mathf.Rad2Deg;
        
        if (CheckSide(u, v) <= 0) {

            return -angle;

        } else {

            return angle;
        }
    }

    private float CheckSide(Vector3 u, Vector3 v) {

        Vector3 w = Vector3.Cross(v, u);

        return w.y;
    }

    public void CheckNpcs() {

        // also counts as List size
        int arraySize = npcsHolder.transform.childCount;

        if (npcs == null)
            npcs = new Transform[arraySize];

        for (int i = 0; i < arraySize; i++) {

            npcs[i] = npcsHolder.GetChild(i).GetComponent<Transform>();
        }

        if (npcMarkers == null)
            npcMarkers = new List<RawImage>(arraySize);

        foreach (Transform t in npcs) {

            if (t.gameObject.activeSelf) {

                npcMarkers.Add(Instantiate(npcMarker, markersParent));
            }
        }
    }

    public void CheckActiveNPCs() {
        if (npcs != null) {
            foreach (Transform t in npcs) {

                if (t != null) {
                    if (t.gameObject.activeSelf) {
                        if (t.gameObject.name == "Russell") {
                            npcMarkers.Add(Instantiate(npcMarker, markersParent));
                        }
                    }
                }
            }
        }
    }

    public void CheckMurders() {

        // also counts as List size
        int arraySize = murdersHolder.transform.childCount;

        murders = new Transform[arraySize];

        for (int i = 0; i < arraySize; i++) {

            murders[i] = murdersHolder.GetChild(i).GetComponent<Transform>();
        }

        murderMarkers = new List<RawImage>(arraySize);

        foreach (Transform t in murders) {

            if (t.gameObject.activeSelf) {

                murderMarkers.Add(Instantiate(murderMarker, markersParent));
            }
        }
    }

    private float UpdateMarkerDirection(Vector3 g) {

        // The same vector but with 'y = 0'
        Vector3 gFloor = new Vector3(g.x, 0, g.z);

        Vector3 v = gFloor - camFloor;

        float devisor = (u.x * v.x) + (u.y * v.y) + (u.z * v.z);

        // M = Magnitude
        float uM = Vector3.Magnitude(u);
        float vM = Vector3.Magnitude(v);

        float dividend = uM * vM;

        // In degrees
        float angle = Mathf.Acos(devisor / dividend) * Mathf.Rad2Deg;

        if (CheckSide(u, v) <= 0) {

            return -angle;

        } else {

            return angle;
        }
    }

    private void ManageCompass() {

        if (inventory.activeSelf || pauseMenu.activeSelf || dialogue.activeSelf || dialogueBox.activeSelf) {

            if (compass.activeSelf) {
                
                compass.SetActive(false);
            }

        } else {

            if (!compass.activeSelf) {
                
                compass.SetActive(true);
            }
        }
    }
}

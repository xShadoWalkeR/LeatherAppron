﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMixerApplication : MonoBehaviour {

    public AudioMixerGroup pauseMixer;

    private AudioSource rainSource;

    // Start is called before the first frame update
    void Start() {

        StartCoroutine(LateStart(2f));
    }

    private IEnumerator LateStart(float waitTime) {

        yield return new WaitForSeconds(waitTime);

        GetAudioSource();
        ApplyMixer();
    }

    private void GetAudioSource() {

        foreach (AudioSource a in gameObject.GetComponents<AudioSource>()) {

            if (a.volume > 0) {

                rainSource = a;
                return;
            }
        }
    }

    private void ApplyMixer() {

        rainSource.outputAudioMixerGroup = pauseMixer;
    }
}

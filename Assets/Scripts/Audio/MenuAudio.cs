﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudio : MonoBehaviour
{
    public AudioClip mouseOver;
    public AudioClip mouseClick;
    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();

    }

    public void OnOver()
    {
        source.clip = mouseOver;
        source.Play();
    }

    public void OnClick()
    {
        source.clip = mouseClick;
        source.Play();
    }
}


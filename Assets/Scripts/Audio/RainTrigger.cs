﻿using UnityEngine;

public class RainTrigger : MonoBehaviour {

    public Transform player;
    public DigitalRuby.RainMaker.BaseRainScript rain;
    public bool checkX;

    private void OnTriggerEnter(Collider other) {
        
        if (other.CompareTag("Player")) {

            if (checkX) {

                if (other.transform.position.x < transform.position.x) {

                    rain.ChangeRainVolume("max");

                } else {

                    rain.ChangeRainVolume("min");
                }

            } else {

                if (other.transform.position.z < transform.position.z) {

                    rain.ChangeRainVolume("max");

                } else {

                    rain.ChangeRainVolume("min");
                }
            }
        }
    }

    private void OnTriggerExit(Collider other) {

        if (other.CompareTag("Player")) {

            if (checkX) {

                if (other.transform.position.x > transform.position.x) {

                    rain.ChangeRainVolume("max");

                } else {

                    rain.ChangeRainVolume("min");
                }

            } else {

                if (other.transform.position.z > transform.position.z) {

                    rain.ChangeRainVolume("max");

                } else {

                    rain.ChangeRainVolume("min");
                }
            }
        }
    }
}

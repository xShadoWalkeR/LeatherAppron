﻿using UnityEngine;

public class AudioManager : MonoBehaviour {

    #region AudioClips

    [Header("Stepping Sounds")]
    public AudioClip[] rugStepsClips;

    public AudioClip[] stoneStepsClips;

    public AudioClip[] tileStepsClips;

    public AudioClip[] woodStepsClips;

    [Header("Landing Sounds")]
    public AudioClip[] landingFeetClips;

    public AudioClip[] landingHandsClips;

    public AudioClip[] landingBreathClips;

    [Header("Roll Sounds")]
    public AudioClip[] rollClips;

    public AudioClip[] rollBreathClips;

    [Header("Climbing Sounds")]
    public AudioClip[] climbingBreathClips;

    public AudioClip[] climbingFeetClips;

    public AudioClip[] grabbingHandsClips;

    public AudioClip[] lettingGoHandsClips;

    [Header("Idle Mouth Sounds")]
    public AudioClip[] breathingClip;

    [Header("Running Mouth Sounds")]
    public AudioClip[] runningBreathClips;

    #endregion

    private string penultimateClipName;
    private string lastClipName;

    private string lastBreath;

    void Start() {

        penultimateClipName = "";
        lastClipName = "";

        lastBreath = "";
    }

    public void PlayStepSound(string tag, AudioSource foot) {

        if (tag == "Rug") {

            PlayClip(foot, GetClip(rugStepsClips));

        } else if (tag == "Stone") {
            
            PlayClip(foot, GetClip(stoneStepsClips));

        } else if (tag == "Tile") {

            PlayClip(foot, GetClip(tileStepsClips));

        } else if (tag == "Wood") {
            
            PlayClip(foot, GetClip(woodStepsClips));
        }
    }

    public void PlayLandFeetSound(AudioSource feet) {

        PlayClip(feet, GetClip(landingFeetClips));
    }

    public void PlayLandHandsSound(AudioSource hands) {

        PlayClip(hands, GetClip(landingHandsClips));
    }

    public void PlayLandBreathClips(AudioSource mouth) {

        PlayClip(mouth, GetClip(landingBreathClips));
    }

    public void PlayRollClips(AudioSource torso) {

        PlayClip(torso, GetClip(rollClips));
    }

    public void PlayRollBreathClips(AudioSource mouth) {

        PlayClip(mouth, GetClip(rollBreathClips));
    }

    public void PlayGrabHandSound(AudioSource hand) {

        PlayClip(hand, GetClip(grabbingHandsClips));
    }

    public void PlayLetGoHandSound(AudioSource hand) {

        PlayClip(hand, GetClip(lettingGoHandsClips));
    }

    public void PlayClimbBreathSound(AudioSource mouth) {

        PlayBreathClip(mouth, climbingBreathClips);
    }

    public void PlayIdleSound(AudioSource mouth) {

        PlayClip(mouth, breathingClip[0]);
    }

    public void PlayRunSound(AudioSource mouth) {

        PlayBreathClip(mouth, runningBreathClips);
    }

    private AudioClip GetClip(AudioClip[] clips) {

        AudioClip retVal = null;

        int length = clips.Length;

        int clipNumber = Random.Range(0, length);   // MIN INCLUSIVE, MAX EXCLUSIVE!!!

        for (int i = 0; i < length; i++) {

            if (i == clipNumber) {

                if (length <= 3) {

                    if (clips[i].name != lastClipName) {

                        SaveLastClips(clips[i].name);
                        return clips[i];

                    } else {

                        retVal = GetClip(clips);
                    }

                } else {

                    if ((clips[i].name != lastClipName) && (clips[i].name != penultimateClipName)) {

                        SaveLastClips(clips[i].name);
                        return clips[i];

                    } else {

                        retVal = GetClip(clips);
                    }
                }
            }
        }

        return retVal;
    }

    private void PlayClip(AudioSource source, AudioClip clip) {
        
        source.clip = clip;
        source.Play();
    }

    private void PlayBreathClip(AudioSource source, AudioClip[] clips) {
        
        foreach (AudioClip a in clips) {

            if (a.name != lastBreath) {
                
                lastBreath = a.name;
                source.clip = a;
                source.Play();
                return;
            }
        }
    }

    private void SaveLastClips(string clipName) {

        penultimateClipName = lastClipName;
        lastClipName = clipName;
    }
}

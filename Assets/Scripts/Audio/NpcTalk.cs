﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcTalk : MonoBehaviour
{
    private AudioSource audio;
    private Animator anim;
    private bool rad;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        audio = GetComponentInParent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (audio.isPlaying && !rad) {
            rad = true;
            anim.SetInteger("Talk", Random.Range(1, 5));
        } else if (!audio.isPlaying) {
            anim.SetInteger("Talk", 0);
            rad = false;
        }
    }
}

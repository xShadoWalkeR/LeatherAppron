﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {

    public AudioManager am;

    [Header("Audio Volume")]
    public float minVolume = 0.3f;
    public float maxVolume = 0.6f;

    [Header("Audio Volume")]         // Also used for the head volume
    public float minHandVol = 0.15f;
    public float maxHandVol = 0.5f;

    private List<AudioSource> feet;
    private List<AudioSource> hands;
    private AudioSource torso;
    private AudioSource head;   // mouth

    private Transform model;

    private StateManager sm;
    private Animator anim;

    private bool breath;   //did he breath while running?

    void OnEnable() {

        feet = new List<AudioSource>(2);
        hands = new List<AudioSource>(2);

        StartCoroutine(LateStart(1f));

        sm = GetComponent<StateManager>();
        anim = GetComponent<Animator>();

        breath = false;
    }

    private IEnumerator LateStart(float waitTime) {

        yield return new WaitForSeconds(waitTime);
        model = transform.GetChild(4);
        GetAudioSources(model);
        ApplyDefaultVolume(feet);
        ApplyDefaultVolume(hands);
        torso.volume = 1f;
        head.volume = minHandVol;
    }

    public void ChangeVolume() {

        if (sm.currentState == State.ThirdPerson) {

            foreach (AudioSource f in feet) {

                f.volume = maxVolume;
            }

            foreach (AudioSource h in hands) {

                h.volume = maxHandVol;
            }
            
            head.volume = maxHandVol-0.15f;

            return;

        } else if (sm.currentState == State.FirstPerson) {

            foreach (AudioSource f in feet) {

                f.volume = minVolume;
            }

            foreach (AudioSource h in hands) {

                h.volume = minHandVol;
            }
            
            head.volume = minHandVol;
        }
    }

    private void GetAudioSources(Transform parent) {

        Transform[] children = parent.GetComponentsInChildren<Transform>();

        foreach (Transform t in children) {
            
            if (t.tag == "Foot") {
                
                feet.Add(t.GetComponent<AudioSource>());
                continue;

            } else if (t.tag == "Hand") {
                
                hands.Add(t.GetComponent<AudioSource>());
                continue;

            } else if (t.tag == "Torso") {

                torso = t.GetComponent<AudioSource>();
                continue;

            } else if (t.tag == "Head") {

                head = t.GetComponent<AudioSource>();
                continue;
            }
        }
    }

    private void ApplyDefaultVolume(List<AudioSource> list) {

        foreach (AudioSource a in list) {

            if (list == hands) {

                a.volume = minHandVol;

            } else {

                a.volume = minVolume;
            }
        }
    }

    public void PlaySound(string bodyPart) {

        switch (bodyPart) {   // or 'action'

            case "climb":
                if (anim.GetBool("Climb")) {

                    //if (anim.GetBool("Jump")) {
                        
                    //    am.PlayClimbBreathSound(head);
                    //}

                    if (hands[0].isPlaying) {
                        
                        am.PlayGrabHandSound(hands[1]);

                    } else {

                        am.PlayGrabHandSound(hands[0]);
                    }

                    anim.ResetTrigger("Climb");
                }
                break;

            case "rightFoot":
                am.PlayStepSound(sm.groundTag, feet[1]);
                break;

            case "leftFoot":
                am.PlayStepSound(sm.groundTag, feet[0]);
                break;

            case "letGoRight":
                am.PlayClimbBreathSound(head);
                am.PlayLetGoHandSound(hands[1]);
                break;

            case "letGoLeft":
                am.PlayLetGoHandSound(hands[0]);
                break;

            case "bothFeet":
                if (feet.Count > 0) am.PlayLandFeetSound(feet[0]);
                break;

            case "bothHands":
                am.PlayLandHandsSound(hands[0]);
                am.PlayLandBreathClips(head);
                break;

            case "torso":
                am.PlayRollBreathClips(head);
                am.PlayRollClips(torso);
                break;

            case "idle":
                if (head != null /*&& !head.isPlaying*/) am.PlayIdleSound(head);
                break;

            case "effort":
                am.PlayClimbBreathSound(head);
                break;

            case "run":
                if (!breath) {

                    am.PlayRunSound(head);
                    breath = true;

                } else {

                    breath = false;
                }
                break;
        }
    }
}

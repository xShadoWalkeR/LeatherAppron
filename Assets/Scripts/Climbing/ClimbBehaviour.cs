﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Climbing {
    public class ClimbBehaviour : MonoBehaviour {
      
        #region Variables
        // Variables to start the behaviour
        public bool climbing;
        public bool lookOnZ;
        public float directThreshold = 1;
        public float minDistance = 2.5f;
        bool hold;
        bool initClimb;
        bool waitToStartClimb;
        bool dropOnLedge;
        bool hasRootmotion;
        bool lookForSpot;
        bool oneCheck;

        // Components
        StateManager states;
        Animator anim;
        ClimbIK ik;

        // Point variables
        Manager curManager;
        Point targetPoint;
        Point curPoint;
        Point prevPoint;
        Neighbour neighbour;
        ConnectionType curConnection;
        GameObject dismountPointGO;
        Neighbour dismountNeighbor;
        Neighbour fallNeighbor;
        Neighbour jumpBackNeighbor;

        // Current and target state
        ClimbStates climbState;
        ClimbStates targetState;

        public enum ClimbStates {
            onPoint,
            betweenPoints,
            inTransit
        }

        #region Curves
        // Variables for our curve movement
        CurvesHolder curvesHolder;
        BezierCurve curCurve;
        #endregion

        // Interpolation variables
        Vector3 _startPos;
        Vector3 _targetPos;
        float _distance;
        float _t;
        bool initTransit;
        bool rootReached;
        bool ikLandSideReached;
        bool ikFollowSideReached;

        // Input variables
        bool lockInput;
        Vector3 inputDirection;
        Vector3 targetPosition;

        // Tweakable varibles
        public Vector3 rootOffset = new Vector3(0, -0.86f, 0); // How much the hips of the animation are above ground
        public float speed_linear = 1.3f;
        public float speed_direct = 2;
        public float speed_dropLedge = 1.5f;

        public AnimationCurve a_jumpingCurve;
        public AnimationCurve a_hangingToBrace;
        public AnimationCurve a_mountCurve;
        public AnimationCurve a_zeroToOne;
        public bool enableRootMovement;
        float _rmMax = 0.25f; // Max fail safe for root movement
        float _rmT;

        LayerMask lm;
        #endregion

        void SetCurveReferences() {
            // Creates a new gameObject wich has all our desired curves in it and assigns them
            GameObject chPrefab = Resources.Load("CurvesHolder") as GameObject;
            GameObject chGO = Instantiate(chPrefab) as GameObject;

            curvesHolder = chGO.GetComponent<CurvesHolder>();
        }

        private void Start() {
            states = GetComponent<StateManager>();
            anim = GetComponentInChildren<Animator>();
            ik = GetComponentInChildren<ClimbIK>();
            SetCurveReferences();
            CreateDirections();

            GameObject disPrefab = Resources.Load("Dismount") as GameObject;
            dismountPointGO = Instantiate(disPrefab) as GameObject;
            dismountNeighbor = new Neighbour();
            dismountNeighbor.target = dismountPointGO.GetComponentInChildren<Point>();
            dismountNeighbor.target.dismountPoint = true;
            dismountNeighbor.cType = ConnectionType.dismount;

            fallNeighbor = new Neighbour();
            fallNeighbor.cType = ConnectionType.fall;

            jumpBackNeighbor = new Neighbour();
            jumpBackNeighbor.cType = ConnectionType.jumpBack;

            lm = (1 << gameObject.layer) | (1 << 3);
            lm = ~lm;
        }

        private void Update() {
            if (!climbing && Input.GetKey(KeyCode.Space)) {
                lookForSpot = true;
            }
        }

        private void FixedUpdate() {
            if (climbing) {
                if (!waitToStartClimb) {
                    if (!hold) {
                        HandleClimbing();
                        InitiateFallOff();
                    }
                } else {
                    InitClimbing(); // Init  the climb behaviour if it's the first time
                    HandleMount();
                }
            } else {
                if (initClimb) {
                    transform.parent = null;
                    anim.SetBool("climbing", false);
                    initClimb = false;
                }
                if (lookForSpot) {
                    lookForSpot = false;
                    LookForClimbSpot();
                    GetComponent<InputHandler>().hMove.HandleJump();
                }
                    
                CharacterOnEdge();
            }
        }

        void LookForClimbSpot() {
            Vector3 origin = transform.position + Vector3.up;
            Vector3 direction = transform.forward;
            RaycastHit hit;

            float maxDistance = 3;
            float maxDistToPoint = 5;

            if (Physics.Raycast(origin, direction, out hit, maxDistance, lm)) {
                Manager tm = hit.transform.GetComponentInChildren<Manager>();
                if (tm != null) {
                    Point closestPoint = tm.ReturnClosest(transform.position);
                    float angle = Vector3.Angle(transform.forward, closestPoint.transform.forward);

                    if (angle > 35) {
                        closestPoint = null;
                        return;
                    }

                    float distanceToPoint = Vector3.Distance(transform.position, closestPoint.transform.parent.position);

                    // Change this to have 3 diferent checks(Now only checks global distance)

                    if (distanceToPoint < maxDistToPoint && states.currentState != State.FirstPerson) { // Check if the the point is in our reach (Hight)
                        hasRootmotion = anim.applyRootMotion; // If the animator had root motion enabled
                        anim.applyRootMotion = false;
                        anim.SetBool("climbing", true);

                        curManager = tm;
                        targetPoint = closestPoint;
                        FixHipPosition(targetPoint);

                        targetPosition = closestPoint.transform.position;
                        curPoint = closestPoint;
                        climbing = true;
                        lockInput = true;
                        targetState = ClimbStates.onPoint;
                        hold = true;
                        states.DisableController();
                        states.climbing = true;
                        waitToStartClimb = true;
                    }
                }
            }
        }

        void CharacterOnEdge() {
            if (!states.isGroundForward) {
                Vector3 origin = transform.position;
                origin += transform.forward;
                origin -= Vector3.up / 3;
                Vector3 direction = transform.position - origin;
                direction.y = 0;
                RaycastHit hit;

                Debug.DrawRay(origin, direction, Color.blue);

                if (Physics.Raycast(origin, direction, out hit, 1, lm)) {
                    if (hit.transform.GetComponentInChildren<Manager>()) {
                        Manager tm = hit.transform.GetComponentInChildren<Manager>();

                        Point closestPoint = tm.ReturnClosest(transform.position);

                        float distanceToPoint = 
                            Vector3.Distance(transform.position, closestPoint.transform.parent.position);

                        if (distanceToPoint < 5 && states.currentState != State.FirstPerson) {
                            if (Input.GetKey(KeyCode.Space)) {
                                curManager = tm;
                                targetPoint = closestPoint;
                                targetPosition = closestPoint.transform.position;
                                curPoint = closestPoint;
                                climbing = true;
                                lockInput = true;
                                dropOnLedge = true;
                                states.DisableController();
                                states.climbing = true;
                                waitToStartClimb = true;
                                anim.SetBool("climbing", true);
                                hasRootmotion = anim.applyRootMotion;
                                anim.applyRootMotion = false;
                                hold = true;
                            }
                            if (!oneCheck && !Input.GetKeyDown(KeyCode.LeftShift)) {
                                oneCheck = true;
                                states.DisableController();
                                states.climbing = true;
                            } else if (states.dummy && Input.GetKeyDown(KeyCode.LeftShift)) {
                                states.climbing = false;
                                states.EnableController();
                            }
                        }
                    }
                }
            } else {
                if (oneCheck) {
                    oneCheck = false;
                    states.climbing = false;
                    states.EnableController();
                }
                    
            }
        }

        void HandleClimbing() {
            if (!lockInput) {
                lookOnZ = Input.GetKey(KeyCode.LeftShift); // Look to jump backwards

                // Handle the input whenever we are not already moving
                inputDirection = Vector3.zero;

                float h = Input.GetAxis("Horizontal");
                float v = Input.GetAxis("Vertical");

                inputDirection = ConvertToInputDirection(h, v); // Convert our input to direction

                if (inputDirection != Vector3.zero) {
                    switch (climbState) {
                        case ClimbStates.onPoint:
                            OnPoint(inputDirection); // If we are on a point
                            break;
                        case ClimbStates.betweenPoints:
                            BetweenPoints(inputDirection); // If we are between points
                            break;
                    }
                }

                // The following lines hardcode the position of the player when he is OnPoint
                // So if the point moves, the player will move along with it
                transform.parent = curPoint.transform.parent;

                if (climbState == ClimbStates.onPoint) {
                    ik.UpdateAllTargetPositions(curPoint);
                    ik.ImmediatePlaceHelpers();
                }
            } else {
                // If we've lock our input that means we are moving
                InTransit(inputDirection);
            }
        }

        Vector3 ConvertToInputDirection(float horizontal, float vertical) {
            int h = (horizontal != 0) ?
                (horizontal < 0) ? -1 : 1
                : 0;
            int v = (vertical != 0) ?
                (vertical < 0) ? -1 : 1
                : 0;

            Vector3 retVal = Vector3.zero;
            retVal.x = h;
            retVal.y = v;

            return retVal;
        }

        void OnPoint(Vector3 inpD) {

            jumpBack = false;
            // Find a neighbour if it exists, towards the desired direction
            neighbour = null;

            Manager targetManager = curManager;

            if (lookOnZ) {
                inpD.z = inpD.y;
                inpD.y = 0;
                targetManager = LookForManagerBehind();

                if (targetManager == null) {
                    jumpBack = true;
                    neighbour = jumpBackNeighbor;
                    jumpBackNeighbor.target = curPoint;
                }
            }

            if (!jumpBack)
                neighbour = ReturnNeighbour(inpD, curPoint, targetManager);

            if (neighbour == null && !lookOnZ) {
                if (inpD.y > 0) {
                    if (CanDismount()) {
                        neighbour = dismountNeighbor;
                    } else {
                        targetManager = LookForManager(transform.up);
                        if (targetManager != null)
                            neighbour = ReturnNeighbour(inpD, curPoint, targetManager);
                    }
                }

                if (inpD.y < 0) {
                    targetManager = LookForManager(-transform.up);
                    if (targetManager != null && targetManager != curManager) {
                        neighbour = ReturnNeighbour(inpD, curPoint, targetManager);
                    } else if (CanFall()) {
                        fallNeighbor.target = curPoint;
                        neighbour = fallNeighbor;
                    }
                }

                if (inpD.x != 0) {
                    Manager managerSides = LookForManagerSides(inpD.x);

                    if (managerSides != null) {
                        neighbour = ReturnNeighbour(inpD, curPoint, managerSides);
                    }
                }
            }

            if (neighbour != null) {
                FixHipPosition(neighbour.target);

                targetPoint = neighbour.target; // Set the neighbour as our target
                prevPoint = curPoint; // The previous point is the one we are currntly in now
                climbState = ClimbStates.inTransit; // Whatever we are doing, our next state would be moving
                UpdateConnectionTransitionByType(neighbour, inpD);
                // Update the variables depending on our connection
                lockInput = true; // We are moving so we don't want any more input while moving
            }
        }

        void BetweenPoints(Vector3 inpD) {

            targetPosition = targetPoint.transform.position;
            climbState = ClimbStates.inTransit;
            targetState = ClimbStates.onPoint;
            prevPoint = curPoint;
            lockInput = true;
            anim.SetBool("Move", false);
            anim.SetTrigger("Climb");
        }

        void UpdateConnectionTransitionByType(Neighbour n, Vector3 inputD) {
            Vector3 desiredPos = Vector3.zero;
            curConnection = n.cType;

            Vector3 direction = targetPoint.transform.position - curPoint.transform.position;
            direction.Normalize();

            switch(n.cType) {
                // If our connection is a 2 step (in between)
                case ConnectionType.inBetween:
                    float distance = Vector3.Distance(curPoint.transform.position, targetPoint.transform.position);
                    desiredPos = curPoint.transform.position + (direction * (distance / 2));
                    // Then our target position is in the middle of two points and a bit back
                    targetState = ClimbStates.betweenPoints; // When the current transition ends, we will be at this state
                    TransitDir transitDir = ReturnTransitDirection(inputD, false);
                    PlayAnim(transitDir);
                    break;
                case ConnectionType.direct: // If it's 1 step (direct) then the curve handles most of the work
                    desiredPos = targetPoint.transform.position;
                    targetState = ClimbStates.onPoint; // We will be again on a point when this ends
                    TransitDir transitDir2 = ReturnTransitDirection(inputD, true);
                    PlayAnim(transitDir2, true);
                    break;
                case ConnectionType.dismount:
                    desiredPos = targetPoint.transform.position;
                    anim.SetInteger("JumpType", 20);
                    anim.SetBool("Move", true);
                    break;
                case ConnectionType.fall:
                    climbing = false;
                    initTransit = false;
                    ik.AddWeightInfluenceAll(0);
                    states.climbing = false;
                    states.EnableController();
                    anim.SetBool("climbing", false);
                    break;
                case ConnectionType.jumpBack:
                    break;
            }

            switch (targetPoint.pointType) {
                case PointType.braced:
                    anim.SetFloat("Stance", 0);
                    break;
                case PointType.hanging:
                    anim.SetFloat("Stance", 1);
                    ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                    ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
                    break;
                default:
                    break;
            }

            targetPosition = desiredPos;
        }

        void InTransit(Vector3 inputD) {
            switch (curConnection) {
                case ConnectionType.inBetween:
                    UpdateLinearVariables();
                    Linear_RootMovement();
                    LerpIKLandingSide_Linear();
                    WrapUp();
                    break;
                case ConnectionType.direct:
                    UpdateDirectVariables(inputDirection);
                    Direct_RootMovement();
                    DirectHandleIK();
                    WrapUp(true);
                    break;
                case ConnectionType.dismount:
                    HandleDismountVariables();
                    Dismount_RootMovement();
                    HandleDismountIK();
                    DismountWrapUp();
                    break;
                case ConnectionType.jumpBack:
                    JumpBackwards();
                    break;
            }
        }

        #region Linear (2 step)
        void UpdateLinearVariables() {
            if (!initTransit) {
                initTransit = true;
                enableRootMovement = true;
                rootReached = false;
                ikFollowSideReached = false;
                ikLandSideReached = false;
                _t = 0;
                _startPos = transform.position;
                _targetPos = targetPosition + rootOffset;
                Vector3 directionToPoint = (_targetPos - _startPos).normalized;

                bool twoStep = (targetState == ClimbStates.betweenPoints);
                Vector3 back = -transform.forward * 0.05f;

                bool diffType = targetPoint.pointType != curPoint.pointType;
                Vector3 down = -transform.up * 0.2f;

                if (diffType) {
                    if (curPoint.pointType != PointType.hanging)
                        diffType = false;
                }

                if (diffType && twoStep)
                    _targetPos += down;
                else if (twoStep)
                    _targetPos += back;

                _distance = Vector3.Distance(_targetPos, _startPos);

                InitIK(directionToPoint, !twoStep);
            }
        }

        void Linear_RootMovement() {
            // Lerp towards target position, this doesn't always lerps towards a target point
            float speed = speed_linear * Time.deltaTime;
            float lerpSpeed = speed / _distance;
            _t += lerpSpeed;

            if (_t > 1) {
                _t = 1;
                rootReached = true;
            }

            Vector3 currentPosition = Vector3.LerpUnclamped(_startPos, _targetPos, _t);
            transform.position = currentPosition;

            HandleRotation();
        }

        void LerpIKLandingSide_Linear() {
            float speed = speed_linear * Time.deltaTime;
            float lerpSpeed = speed / _distance;

            _ikT += lerpSpeed * 3;

            if (_ikT > 1) {
                _ikT = 1;
                ikLandSideReached = true;
            }

            Vector3 ikPosition = Vector3.LerpUnclamped(_ikStartPos[0], _ikTargetPos[0], _ikT);
            ik.UpdateTargetPosition(ik_L, ikPosition);

            _fikT += lerpSpeed * 2;
            if (_fikT > 1) {
                _fikT = 1;
                ikFollowSideReached = true;
            }

            if (targetPoint.pointType == PointType.hanging) {
                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
            } else {
                Vector3 followSide = Vector3.LerpUnclamped(_ikStartPos[1], _ikTargetPos[1], _fikT);
                ik.UpdateTargetPosition(ik_F, followSide);
            }
        }
        #endregion

        #region Direct (1 step)
        void UpdateDirectVariables(Vector3 inpD) {
            if (!initTransit) {
                initTransit = true;
                enableRootMovement = false;
                rootReached = false;
                ikFollowSideReached = false;
                ikLandSideReached = false;
                _t = 0;
                _rmT = 0;
                _targetPos = targetPosition + rootOffset;
                _startPos = transform.position;

                // If we're going vertical we're using a different curve than horizontal
                bool vertical = (Mathf.Abs(inpD.y) > 0.1f);
                curCurve = FindCurveByInput(vertical, inpD);
                curCurve.transform.rotation = curPoint.transform.rotation;

                // Set the first point of the curve on the starting position
                // And the last on the target.
                BezierPoint[] points = curCurve.GetAnchorPoints();
                points[0].transform.position = _startPos;
                points[points.Length - 1].transform.position = _targetPos;

                InitIK_Direct(inputDirection);

            }
        }

        BezierCurve FindCurveByInput(bool vertical, Vector3 inpd) {
            BezierCurve retVal = null;

            if (!vertical) {
                if (inpd.x > 0) {
                    retVal = curvesHolder.ReturnCurve(CurveType.right);
                } else {
                    retVal = curvesHolder.ReturnCurve(CurveType.left);
                }
            } else {
                if (inpd.y > 0) {
                    retVal = curvesHolder.ReturnCurve(CurveType.up);
                } else {
                    if (Input.GetKey(KeyCode.LeftShift))
                        retVal = curvesHolder.ReturnCurve(CurveType.back); // Fast Patch change later!
                    else
                        retVal = curvesHolder.ReturnCurve(CurveType.down);
                }
            }

            return retVal;
        }

        void Direct_RootMovement() {
            // Instead of lerping to the target position we let the curve handle that
            // And all the positions between them
            if (enableRootMovement) {
                _t += Time.deltaTime * speed_direct;
            } else {
                if (_rmT < _rmMax)
                    _rmT += Time.deltaTime;
                else
                    enableRootMovement = true;
            }

            if (_t > 0.95f) {
                _t = 1;
                rootReached = true;
            }

            if (!lookOnZ)
                HandleWeightAll(_t, a_jumpingCurve);
            else
                HandleWeightAll(_t, a_mountCurve);

            Vector3 targetPos = curCurve.GetPointAt(_t);
            transform.position = targetPos;

            HandleRotation();
        }

        void DirectHandleIK() {
            if (inputDirection.y != 0) {
                LerpIKHands_Direct();
                LerpIKFeet_Direct();
            } else {
                LerpIKLandingSide_Direct();
                LerpIKFollowSide_Direct();
            }
        }

        #region IK Direct
        void LerpIKHands_Direct() {
            if (enableRootMovement)
                _ikT += Time.deltaTime * 5;

            if (_ikT > 1) {
                _ikT = 1;
                ikLandSideReached = true;
            }

            Vector3 lhPosition = Vector3.LerpUnclamped(_ikStartPos[0], _ikTargetPos[0], _ikT);
            ik.UpdateTargetPosition(AvatarIKGoal.LeftHand, lhPosition);

            Vector3 rhPosition = Vector3.LerpUnclamped(_ikStartPos[2], _ikTargetPos[2], _ikT);
            ik.UpdateTargetPosition(AvatarIKGoal.RightHand, rhPosition);
        }

        void LerpIKFeet_Direct() {
            if (targetPoint.pointType == PointType.hanging) {
                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
            } else {
                if (enableRootMovement)
                    _fikT += Time.deltaTime * 5;

                if (_fikT > 1) {
                    _fikT = 1;
                    ikFollowSideReached = true;
                }

                Vector3 lfPosition = Vector3.LerpUnclamped(_ikStartPos[1], _ikTargetPos[1], _fikT);
                ik.UpdateTargetPosition(AvatarIKGoal.LeftFoot, lfPosition);

                Vector3 rfPosition = Vector3.LerpUnclamped(_ikStartPos[3], _ikTargetPos[3], _fikT);
                ik.UpdateTargetPosition(AvatarIKGoal.RightFoot, rfPosition);
            }
        }

        void LerpIKLandingSide_Direct() {
            if (enableRootMovement)
                _ikT += Time.deltaTime * 3.2f;

            if (_ikT > 1) {
                _ikT = 1;
                ikLandSideReached = true;
            }

            Vector3 landPosition = Vector3.LerpUnclamped(_ikStartPos[0], _ikTargetPos[0], _ikT);
            ik.UpdateTargetPosition(ik_L, landPosition);

            if (targetPoint.pointType == PointType.hanging) {
                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
            } else {
                Vector3 followPosition = Vector3.LerpUnclamped(_ikStartPos[1], _ikTargetPos[1], _ikT);
                ik.UpdateTargetPosition(ik_F, followPosition);
            }
        }

        void LerpIKFollowSide_Direct() {
            if (enableRootMovement)
                _fikT += Time.deltaTime * 2.6f;

            if (_fikT > 1) {
                _fikT = 1;
                ikFollowSideReached = true;
            }

            Vector3 landPosition = Vector3.LerpUnclamped(_ikStartPos[2], _ikTargetPos[2], _fikT);
            ik.UpdateTargetPosition(ik.ReturnOppositeIK(ik_L), landPosition);

            if (targetPoint.pointType == PointType.hanging) {
                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
            } else {

                Vector3 followPosition = Vector3.LerpUnclamped(_ikStartPos[3], _ikTargetPos[3], _fikT);
                ik.UpdateTargetPosition(ik.ReturnOppositeIK(ik_F), followPosition);
            }
        }
        #endregion

        #endregion

        #region Mount
        void InitClimbing() {
            if (!initClimb) {
                initClimb = true;

                if (ik != null) { // Update ik positions and targets
                    ik.UpdateAllPointsOnOne(targetPoint);
                    ik.UpdateAllTargetPositions(targetPoint);
                    ik.ImmediatePlaceHelpers();
                }

                // Our connection type
                curConnection = ConnectionType.direct;
                // The state we will be when our current one ends
                targetState = ClimbStates.onPoint;
                anim.SetBool("Move", false);
                anim.SetTrigger("Climb");
                anim.SetInteger("JumpType", 0);
                hold = true;
            }
        }

        void HandleMount() {
            if (!initTransit) {
                initTransit = true;
                enableRootMovement = false;
                ikFollowSideReached = false;
                ikLandSideReached = false;
                _t = 0;
                _startPos = transform.position;
                _targetPos = targetPosition + rootOffset;

                curCurve = (dropOnLedge) ? curvesHolder.ReturnCurve(CurveType.dropLedge)
                    : curvesHolder.ReturnCurve(CurveType.mount);

                curCurve.transform.rotation = targetPoint.transform.rotation;
                BezierPoint[] points = curCurve.GetAnchorPoints();
                points[0].transform.position = _startPos;
                points[points.Length - 1].transform.position = _targetPos;

                if (dropOnLedge)
                    anim.CrossFade("dropLedge", 0.4f);

                anim.SetFloat("Stance", (targetPoint.pointType == PointType.braced) ? 0 : 1);
                
                dropOnLedge = false;
            }

            if (enableRootMovement)
                _t += Time.deltaTime * 2;

            if (_t >= 0.99f) {
                _t = 1;
                waitToStartClimb = false;
                lockInput = false;
                initTransit = false;
                ikLandSideReached = false;
                climbState = targetState;
            }

            Vector3 targetPos = curCurve.GetPointAt(_t);
            transform.position = targetPos;
            HandleWeightAll(_t, a_mountCurve);
            HandleRotation();
        }

        #endregion

        #region Dismount
        void HandleDismountVariables() {
            if (!initTransit) {
                initTransit = true;
                enableRootMovement = false;
                rootReached = false;
                ikLandSideReached = false;
                ikFollowSideReached = false;
                _t = 0;
                _rmT = 0;
                _startPos = transform.position;
                _targetPos = targetPosition;

                curCurve = curvesHolder.ReturnCurve(CurveType.dismount);
                BezierPoint[] points = curCurve.GetAnchorPoints();
                curCurve.transform.rotation = transform.rotation;
                points[0].transform.position = _startPos;
                points[points.Length - 1].transform.position = _targetPos;

                _ikT = 0;
                _fikT = 0;
            }
        }

        void Dismount_RootMovement() {
            if (enableRootMovement)
                _t += Time.deltaTime / 2;

            if (_t >= 0.99f) {
                _t = 1;
                rootReached = true;
            }

            Vector3 targetPos = curCurve.GetPointAt(_t);
            transform.position = targetPos;
        }

        void HandleDismountIK() {
            if (enableRootMovement)
                _ikT += Time.deltaTime * 3;

            _fikT += Time.deltaTime * 2;

            HandleIKWeight_Dismount(_ikT, _fikT, 1, 0);
        }

        void HandleIKWeight_Dismount(float ht, float ft, float from, float to) {
            float t1 = ht * 3;

            if (t1 > 1) {
                t1 = 1;
                ikLandSideReached = true;
            }

            float handsWeight = Mathf.Lerp(from, to, t1);
            ik.InfluenceWeight(AvatarIKGoal.LeftHand, handsWeight);
            ik.InfluenceWeight(AvatarIKGoal.RightHand, handsWeight);

            float t2 = ft * 1;

            if (t2 > 1) {
                t2 = 1;
                ikFollowSideReached = true;
            }

            float feetWeight = Mathf.Lerp(from, to, t2);
            ik.InfluenceWeight(AvatarIKGoal.LeftFoot, feetWeight);
            ik.InfluenceWeight(AvatarIKGoal.RightFoot, feetWeight);
        }

        void DismountWrapUp() {
            if (rootReached) {
                climbing = false;
                initTransit = false;
                anim.SetBool("climbing", false);
                states.climbing = false;
                states.EnableController();
            }
        }
        #endregion

        #region Falloff
        void InitiateFallOff() {
            if (climbState == ClimbStates.onPoint && !lockInput) {
                if (Input.GetKeyUp(KeyCode.X)) {
                    climbing = false;
                    initTransit = false;
                    ik.AddWeightInfluenceAll(0);
                    states.climbing = false;
                    states.EnableController();
                    anim.SetBool("climbing", false);
                }
            }
        }
        #endregion

        #region Universal

        bool waitForWrapUp;

        void WrapUp(bool direct = false) {
            if (rootReached) {
                if (!anim.GetBool("Jump")) {
                    if (!waitForWrapUp) {
                        StartCoroutine(WrapUpTransition(0.05f));
                        waitForWrapUp = true;
                    }
                }
            }
        }

        IEnumerator WrapUpTransition(float t) {
            yield return new WaitForSeconds(t);
            climbState = targetState; // Set our current state

            if (climbState == ClimbStates.onPoint)
                curPoint = targetPoint; // Update to target point only if we are not inbeteewn

            // Reset variables
            initTransit = false;
            lockInput = false;
            inputDirection = Vector3.zero;
            waitForWrapUp = false;
        }

        void HandleRotation() {
            Vector3 targetDir = targetPoint.transform.forward;

            if (targetDir == Vector3.zero)
                targetDir = transform.forward;

            Quaternion targetRot = Quaternion.LookRotation(targetDir);

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * 5);
        }

        #endregion

        #region IK
        AvatarIKGoal ik_L; // ik for the landing side
        AvatarIKGoal ik_F; // ik for the following side
        float _ikT;
        float _fikT;
        Vector3[] _ikStartPos = new Vector3[4];
        Vector3[] _ikTargetPos = new Vector3[4];

        void InitIK(Vector3 directionToPoint, bool opposite) {
            Vector3 relativeDirection = transform.InverseTransformDirection(directionToPoint);

            if (Mathf.Abs(relativeDirection.y) > 0.5f) {
                float targetAnim = 0;

                if (targetState == ClimbStates.onPoint) {
                    ik_L = ik.ReturnOppositeIK(ik_L);
                } else {
                    if (Mathf.Abs(relativeDirection.x) > 0) {
                        if (relativeDirection.x < 0)
                            ik_L = AvatarIKGoal.LeftHand;
                        else
                            ik_L = AvatarIKGoal.RightHand;
                    }

                    targetAnim = (ik_L == AvatarIKGoal.RightHand) ? 1 : 0;
                    if (relativeDirection.y < 0)
                        targetAnim = (ik_L == AvatarIKGoal.RightHand) ? 0 : 1;

                    anim.SetFloat("Movement", targetAnim);
                }
            } else {
                ik_L = (relativeDirection.x < 0) ? AvatarIKGoal.LeftHand : AvatarIKGoal.RightHand;

                if (opposite) {
                    ik_L = ik.ReturnOppositeIK(ik_L);
                }
            }

            _ikT = 0;
            UpdateIKTarget(0, ik_L, targetPoint);

            ik_F = ik.ReturnOppositeLimb(ik_L);
            _fikT = 0;
            UpdateIKTarget(1, ik_F, targetPoint);
        }

        void InitIK_Direct(Vector3 directionToPoint) {
            if (directionToPoint.y != 0) {
                _fikT = 0;
                _ikT = 0;

                UpdateIKTarget(0, AvatarIKGoal.LeftHand, targetPoint);
                UpdateIKTarget(1, AvatarIKGoal.LeftFoot, targetPoint);

                UpdateIKTarget(2, AvatarIKGoal.RightHand, targetPoint);
                UpdateIKTarget(3, AvatarIKGoal.RightFoot, targetPoint);
            } else {
                InitIK(directionToPoint, false);
                InitIKOpposite();
            }
        }

        void InitIKOpposite() {
            UpdateIKTarget(2, ik.ReturnOppositeIK(ik_L), targetPoint);
            UpdateIKTarget(3, ik.ReturnOppositeIK(ik_F), targetPoint);
        }

        void UpdateIKTarget(int posIndex, AvatarIKGoal _ikGoal, Point tp) {
            _ikStartPos[posIndex] = ik.ReturnCurrentPointPosition(_ikGoal);
            _ikTargetPos[posIndex] = tp.ReturnIK(_ikGoal).target.transform.position;
            ik.UpdatePoint(_ikGoal, tp);
        }

        void HandleWeightAll(float t, AnimationCurve aCurve) {
            float inf = aCurve.Evaluate(t);
            ik.AddWeightInfluenceAll(1 - inf);

            // Close the ik for the feet if going from hanging to braced
            if (curPoint.pointType == PointType.hanging && targetPoint.pointType == PointType.braced) {
                float inf2 = a_zeroToOne.Evaluate(t);

                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, inf2);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, inf2);
            }

            if (curPoint.pointType == PointType.hanging && targetPoint.pointType == PointType.hanging) {
                ik.InfluenceWeight(AvatarIKGoal.LeftFoot, 0);
                ik.InfluenceWeight(AvatarIKGoal.RightFoot, 0);
            }
        }
        #endregion

        #region Animations
        TransitDir ReturnTransitDirection(Vector3 inpd, bool jump) {
            TransitDir retVal = default(TransitDir);

            float targetAngle = Mathf.Atan2(inpd.x, inpd.y) * Mathf.Rad2Deg;

            if (!jump) {
                if (Mathf.Abs(inpd.y) > 0) {
                    retVal = TransitDir.m_vert;
                } else {
                    retVal = TransitDir.m_hor;
                }
            } else {
                if (targetAngle < 22.5f && targetAngle > -22.5f) {
                    retVal = TransitDir.j_up;
                } else if (targetAngle < 180 + 22.5f && targetAngle > 180 - 22.5f) {
                    retVal = TransitDir.j_down;
                } else if (targetAngle < 90 + 22.5f && targetAngle > 90 - 22.5f) {
                    retVal = TransitDir.j_right;
                } else if (targetAngle < -90 + 22.5f && targetAngle > -90 - 22.5f) {
                    retVal = TransitDir.j_left;
                }

                if (Mathf.Abs(inpd.y) > Mathf.Abs(inpd.x)) {
                    if (inpd.y < 0)
                        retVal = TransitDir.j_down;
                    else
                        retVal = TransitDir.j_up;
                }
            }

            if (lookOnZ) {
                if (inpd.z < 0) {
                    retVal = TransitDir.j_back;
                }
            }

            return retVal;
        }

        void PlayAnim(TransitDir dir, bool jump = false) {
            // 6 = Move vertical; 5 = Move horizontal; 0 = Up; 1 = Down; 2 = Right; 3 = Left

            int target = 0;

            switch(dir) {
                case TransitDir.m_hor:
                    target = 5;
                    break;
                case TransitDir.m_vert:
                    target = 6;
                    break;
                case TransitDir.j_up:
                    target = 0;
                    break;
                case TransitDir.j_down:
                    target = 1;
                    break;
                case TransitDir.j_left:
                    target = 3;
                    break;
                case TransitDir.j_right:
                    target = 2;
                    break;
                case TransitDir.j_back:
                    target = 0; // Change here when getting a new animation for this!!
                    break;
            }

            anim.SetInteger("JumpType", target);

            if (!jump) {

                anim.SetBool("Move", true);
                anim.SetTrigger("Climb");
            } 
            else
                anim.SetBool("Jump", true);
                anim.SetTrigger("Climb");
        }

        enum TransitDir {
            m_hor,
            m_vert,
            j_up,
            j_down,
            j_left,
            j_right,
            j_back,
            j_forward
        }
        #endregion

        #region NeighbourManager
        Vector3[] availableDirections; // Raised to 10

        void CreateDirections() {
            availableDirections = new Vector3[10];
            availableDirections[0] = new Vector3(1, 0, 0);
            availableDirections[1] = new Vector3(-1, 0, 0);
            availableDirections[2] = new Vector3(0, 1, 0);
            availableDirections[3] = new Vector3(0, -1, 0);
            availableDirections[4] = new Vector3(-1, -1, 0);
            availableDirections[5] = new Vector3(1, 1, 0);
            availableDirections[6] = new Vector3(1, -1, 0);
            availableDirections[7] = new Vector3(-1, 1, 0);
            availableDirections[8] = new Vector3(0, 0, -1);
            availableDirections[9] = new Vector3(0, 0, 1);
        }

        Neighbour ReturnNeighbour(Vector3 inpd, Point curPoint, Manager m) {
            if (m == null)
                return null;

            Neighbour retVal = null;
            Neighbour n = new Neighbour();

            curManager = m;

            Point tp = NeighbourPoint(inpd, curPoint, curManager);

            if (tp == null)
                return null;

            n.target = tp;

            float distance = Vector3.Distance(curPoint.transform.position, tp.transform.position);

            if (!lookOnZ) {
                if (distance < minDistance) {
                    if (inpd.y != 0)
                        n.cType = (distance < (directThreshold * 0.80f)) ? ConnectionType.inBetween : ConnectionType.direct;
                    else
                        n.cType = (distance < directThreshold) ? ConnectionType.inBetween : ConnectionType.direct;

                    if (n.cType == ConnectionType.direct && inpd.x != 0 && inpd.y != 0) {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                n.cType = ConnectionType.direct;
            }

            retVal = n;

            return retVal;
        }

        Point NeighbourPoint(Vector3 targetDirection, Point from, Manager m) {
            Point retVal = null;

            List<Point> canidates = CanidatePointsOnDirection(targetDirection, from, m);

            retVal = ReturnClosest(from, canidates);

            return retVal;
        }

        List<Point> CanidatePointsOnDirection(Vector3 targetDirection, Point from, Manager m) {
            List<Point> retVal = new List<Point>();

            for (int p = 0; p < m.allPoints.Count; p++) {
                Point targetPoint = m.allPoints[p];

                if (targetPoint == from)
                    continue;

                Vector3 relativePosition = from.transform.InverseTransformPoint(targetPoint.transform.position);

                if (IsDirectionValid(targetDirection, relativePosition)) {
                    retVal.Add(targetPoint);
                }
            }

            return retVal;
        }

        bool IsDirectionValid(Vector3 targetDirection, Vector3 canidate) {
            bool retVal = false;

            float targetAngle = Mathf.Atan2(targetDirection.x, targetDirection.y) * Mathf.Rad2Deg;
            float angle = Mathf.Atan2(canidate.x, canidate.y) * Mathf.Rad2Deg;

            if (targetDirection.y != 0) {
                targetAngle = Mathf.Abs(targetAngle);
                angle = Mathf.Abs(angle);
            }

            if (targetDirection.z != 0) {
                targetAngle = Mathf.Abs(Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg);
                angle = Mathf.Abs(Mathf.Atan2(canidate.x, canidate.z) * Mathf.Rad2Deg);

                if (angle < targetAngle + 22.5f && angle > targetAngle - 22.5f) {
                    retVal = true;
                }
            } else {
                if (angle < targetAngle + 22.5f && angle > targetAngle - 22.5f) {
                    retVal = true;
                }
            }

            return retVal;
        }

        Point ReturnClosest(Point cp, List<Point> l) {
            Point retVal = null;
            float minDist = Mathf.Infinity;

            for (int i = 0; i < l.Count; i++) {
                float tempDist = Vector3.Distance(cp.transform.position, l[i].transform.position);

                if (tempDist < minDist && l[i] != cp) {
                    minDist = tempDist;
                    retVal = l[i];
                }
            }

            return retVal;
        }

        Manager LookForManagerBehind() {
            Manager retVal = null;

            RaycastHit hit;
            Vector3 origin = transform.position;
            Vector3 direction = -transform.forward;

            Debug.DrawRay(origin, direction * 5);

            if (Physics.Raycast(origin, direction, out hit, 5, lm)) {
                if (hit.transform.root.GetComponentInChildren<Manager>()) {
                    retVal = hit.transform.root.GetComponentInChildren<Manager>();
                }
            }

            return retVal;
        }

        bool CanDismount() {
            bool retVal = false;

            Vector3 worldP = curPoint.transform.position;
            Vector3 aboveOrigin = worldP - transform.position;

            bool above = GetHit(aboveOrigin, Vector3.up, 1.5f, lm);

            Debug.DrawRay(aboveOrigin, Vector3.up * 1.5f, Color.red);

            if (!above) {
                Vector3 forwardOrigin = aboveOrigin + Vector3.up * 2;
                Debug.DrawRay(forwardOrigin, transform.forward * 2, Color.yellow);

                bool forward = GetHit(forwardOrigin, transform.forward, 2, lm);

                if (!forward) {
                    Vector3 disOrigin = worldP + (transform.forward + Vector3.up * 2);
                    RaycastHit hit;

                    Debug.DrawRay(disOrigin, -Vector3.up * 2, Color.green);

                    if (Physics.Raycast(disOrigin, -Vector3.up * 2, out hit, 2)) {
                        Vector3 gp = hit.point;
                        gp.y += 0.04f + Mathf.Abs(dismountNeighbor.target.transform.localPosition.y);
                        dismountPointGO.transform.position = gp;
                        dismountPointGO.transform.rotation = transform.rotation;
                        retVal = true;
                    }
                }
            }

            return retVal;
        }

        bool CanFall() {
            bool retVal = false;

            RaycastHit hit;

            if (Physics.Raycast(curPoint.transform.position, -Vector3.up, out hit, 3, lm)) {
                retVal = true;
            }

            return retVal;
        }

        bool GetHit(Vector3 origin, Vector3 direction, float dis, LayerMask lm) {
            bool retVal = false;

            RaycastHit hit;

            if (Physics.Raycast(origin, direction, out hit, dis, lm)) {
                retVal = true;
            }

            return retVal;
        }

        void FixHipPosition(Point p) {
            if (p.pointType == PointType.hanging) {
                Vector3 targetP = Vector3.zero;
                targetP.y = -1.5f;
                p.transform.localPosition = targetP;
            }

            if (p.pointType == PointType.braced) {
                Vector3 targetP = Vector3.zero;
                targetP.y = -1.06f;
                targetP.z = -0.373f;
                p.transform.localPosition = targetP;
            }
        }

        Manager LookForManagerSides(float x) {
            Manager retval = null;

            RaycastHit hit;
            Vector3 origin = transform.position + -transform.forward;
            Vector3 direction = transform.right * x;

            Debug.DrawRay(origin, direction * 5);

            bool hitSides = GetHit(origin, direction, 2, lm);

            if (hitSides == false) {
                Vector3 towardsOrigin = origin + direction * 5;

                Debug.DrawRay(towardsOrigin, transform.forward * 5);

                if (Physics.Raycast(towardsOrigin, transform.forward, out hit, 5, lm)) {
                    if (hit.transform.GetComponentInChildren<Manager>()) {
                        retval = hit.transform.GetComponentInChildren<Manager>();
                    }
                }
            }

            return retval;
        }

        Manager LookForManager(Vector3 direction) {
            Manager retVal = null;

            Vector3 worldP = curPoint.transform.position;
            Vector3 aboveOrigin = worldP - transform.forward;

            bool above = GetHit(aboveOrigin, direction, 1.5f, lm);

            Debug.DrawRay(aboveOrigin, direction * 1.5f, Color.red);

            if (!above) {
                Vector3 forwardOrigin = aboveOrigin + direction * 2;

                Debug.DrawRay(forwardOrigin, transform.forward * 2, Color.yellow);
                RaycastHit hit;

                if (Physics.Raycast(forwardOrigin, transform.forward, out hit, 2, lm)) {
                    if (hit.transform.GetComponentInChildren<Manager>()) {
                        retVal = hit.transform.GetComponentInChildren<Manager>();
                    }
                }
            }

            return retVal;
        }
        #endregion

        #region JumpBack
        float rotationT;
        float targetAngle;
        bool jumpBack;
        bool init_JumpBack;

        void JumpBackwards() {
            if (!init_JumpBack) {
                anim.applyRootMotion = false;
                init_JumpBack = true;
                ik.AddWeightInfluenceAll(0);
                states.climbing = false;
                states.EnableController();
                states.rBody.isKinematic = true;
                anim.SetBool("Jump", true);
                anim.SetTrigger("Climb");
                anim.SetInteger("JumpType", 22);
                enableRootMovement = false;
                rotationT = 0;
                targetAngle = transform.localRotation.y + 720; // Works no clue why 0o
            }

            if (enableRootMovement) {
                rotationT += Time.deltaTime * 2;
            }

            transform.localRotation = Quaternion.Euler(transform.localRotation.x,
                Mathf.Lerp(transform.localRotation.y, targetAngle, rotationT),
                transform.localRotation.z);
        }

        void AddJumpBackWardsForce() {
            states.rBody.isKinematic = false;
            StartCoroutine(JumpBackForce());
            enableRootMovement = true;
        }

        IEnumerator JumpBackForce() {
            yield return new WaitForEndOfFrame();
            states.rBody.AddForce(-transform.forward * 5 + Vector3.up * 2, ForceMode.Impulse);
        }

        void StopClimbing() {
            states.climbing = false;
            states.EnableController();
            climbing = false;
            anim.SetBool("climbing", false);
            anim.SetBool("Jump", false);
            init_JumpBack = false;
            jumpBack = false;
        }
        #endregion

        public void UnHold() {
            hold = false;
        }

        public void EnableRootmovement() {
            enableRootMovement = true;
        }
        
    }
}
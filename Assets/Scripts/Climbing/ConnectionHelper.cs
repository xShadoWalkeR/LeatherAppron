﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Climbing {
    [ExecuteInEditMode]
    public class ConnectionHelper : MonoBehaviour {

        public Point targetPoint;
        public Vector3 targetDirection; // Direction from this point to the other
        public Vector3 thisTargetDirection; // Direction from the other point to this one
        public ConnectionType connectionType;
        public bool addAsCustom;
        public bool makeConnection;
        
/*        void Update() {
            if (makeConnection) {
                makeConnection = false;

                CreateConnection();
                targetPoint = null;
            }
        }*/

        void CreateConnection() {
            Point thisPoint = GetComponent<Point>();

            if (thisPoint == null || targetPoint == null) {
                Debug.Log("One of the points is null!");
                return;
            }

            // Add a point connection from the current point to the target point
            Neighbour n1 = new Neighbour();
            n1.target = targetPoint;
            n1.direction = targetDirection;
            n1.customConnection = addAsCustom;
            n1.cType = connectionType;

            thisPoint.neighbours.Add(n1);

            // Add a point connection from the targetPoint to the current point
            Neighbour n2 = new Neighbour();
            n2.target = thisPoint;
            n2.direction = -targetDirection;
            n2.customConnection = addAsCustom;
            n2.cType = connectionType;

            targetPoint.neighbours.Add(n2);

            UnityEditor.EditorUtility.SetDirty(thisPoint);
            UnityEditor.EditorUtility.SetDirty(targetPoint);
        }
    }
}
#endif
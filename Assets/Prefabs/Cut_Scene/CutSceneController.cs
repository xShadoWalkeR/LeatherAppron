﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneController : MonoBehaviour
{

    public GameObject player;
    public GameObject playerCam;

    private Animator anim;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 10.3) {
            anim.speed = 1f;
            anim.SetBool("Wake", true);
        } else if (timer >= 4) {
            anim.SetBool("Seizure", true);
            anim.speed = 0.8f;
        }
    }
}
